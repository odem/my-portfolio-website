<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';

/**
 * Sends email to shana@shanaodem.com if user data is valid, and if data
 * submission was sent via AJAX, an array of response data is sent back,
 * otherwise a status string is sent back.
 *
 * @return mixed[] | string $response_data - if ajax submission, returns
 *     $response_data with properties: $errors an array containing field names
 *     that contain invalid data, and a boolean $success indicating if the email
 *     was sent, otherwise a string stating the status of the submission is
 *     returned.
 *
 * @see {@link validate_data} for further details on user data submission and
 * evaluation.
 */
function mailer() {
    /** @var PHPMailer setup. */
    $mail = new PHPMailer;

    /** @var array holds valid user data for each required email field. */
    $fields = [
        "name" => '',
        "email" => '',
        "subject" => '',
        "message" => '',
    ];

    /** @var array holds field names containing invalid data. */
    $error_fields = [];

    /** @var boolean indicates if email was sent. */
    $mailed = false;

    /** @var boolean indicates if submission was sent using AJAX or not. */
    $ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

    /**
     * Returns a boolean indicating if data is valid or not.
     *
     * Data must be sent as POST. If submission used AJAX, user data must
     * wrapped together and attached to a "formData" property. User data is
     * valid if all properties in $fields are non-empty, and the email field
     * contains a valid email.
     * @param  array &$fields - reference to required email fields.
     * @param  array &$error_fields - reference to fields name associated with
     *     errors.
     * @param  boolean $ajax - indicate if submission used AJAX or not.
     *
     * @return boolean - TRUE if user data is valid, otherwise FALSE.
     */
    function validate_data(&$fields, &$error_fields, $ajax) {
        $data_valid = true;
        $raw_data = [];

        if (isset($_POST["formData"]) || !empty($_POST)) {
            if ($ajax) {
                $raw_data = json_decode($_POST["formData"], true);
            } else {
                $raw_data = $_POST;
            }

            foreach($fields as $key => $value) {
                if (isset($raw_data[$key])) {
                    $data = htmlspecialchars(trim($raw_data[$key]));
                    $is_email = ($key === "email");
                    $is_valid_email =
                        (($data === filter_var($data, FILTER_SANITIZE_EMAIL)) &&
                        ($data === filter_var($data, FILTER_VALIDATE_EMAIL)));

                    if(empty($data) || ($is_email && !$is_valid_email)) {
                        $error_fields[] = $key;
                        $data_valid = false;
                    } else {
                        $fields[$key] = $data;
                    }
                } else {
                    $data_valid = false;
                }
            }
        } else {
            $data_valid = false;
        }

        return $data_valid;
    }

    /**
     * Creates and returns the body of the email to be sent.
     *
     * @param  array &$fields - reference to required email fields.
     *
     * @return string - non-empty string containing html for the body of the
     *     email to be sent.
     */
    function create_emailbody(&$fields) {
        $email_body = "<body>";
        $email_body .= "<h1>New Message From Portfolio Contact Form</h1>";
        $email_body .= "<table>";
        $email_body .= '<tr><th>Name: </th><td>' . $fields["name"] . '</td>';
        $email_body .= '<tr><th>Email: </th><td>' . $fields["email"] . '</td>';
        $email_body .= '<tr><th>Message: </th><td>' . $fields["message"] .
            '</td>';
        $email_body .= "</table>";
        $email_body .= "</body>";

        return $email_body;
    }

    /**
     * Returns an encoded JSON string of the response data.
     *
     * @param  array &$error_fields - reference to fields name associated with
     *     errors.
     * @param  boolean $ajax - indicate if submission used AJAX or not.
     *
     * @return string - encoded JSON string of $response_data.
     */
    function response_data(&$error_fields, $mail_sent) {
        $response_data = [
            "errors" => $error_fields,
            "success" => $mail_sent,
        ];

        return json_encode($response_data);
    }

    /** Program starts. */
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $data_valid = validate_data($fields, $error_fields, $ajax);
    } else {
        $data_valid = false;
    }

    if ($data_valid) {
        $recipient = "shana@shanaodem.com";
        $subject = 'Portfolio Site Message: ' . $fields["subject"];
        $email_content = create_emailbody($fields);

        /** Email headers. */
        $mail->setFrom($recipient, 'Portfolio Site Contact Form');
        $mail->addAddress($recipient);
        $mail->addReplyTo($fields["email"], $fields["name"]);

        /** Email content. */
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $email_content;

        $mailed = $mail->send();
    }

    if ($ajax) {
        header('Content-Type: application/json');
        echo response_data($error_fields, $mailed);
    } else {
        if (!empty($error_fields)) {
            echo "There was an input error. Please check your input " .
                "information and re-submit form. ";
        } else {
            if (!$mailed) {
                echo "There was an error while submitting the form. " .
                    "Please try again later. ";
            } else {
                echo "Contact form successfully submitted. Thank you, " .
                    "I will get back to you soon. ";
            }
        }

        if ($mailed && empty($error_fields)) {
            echo "<br><a href=http://whineycataudio.com/9098631/#SxnContact>" .
                "Go Back to Site</a>";
        } else {
            echo "<br>Use browser's back button to go back to site.";
        }
    }
}

mailer();
?>