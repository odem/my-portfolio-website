/** insertCopyright =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="xhr200NoErrorSuc"></div>' +
    '<div id="xhr200NoErrorSuc2"></div>' +
    '<div id="xhr200NoErrorSuc3"></div>' +
    '<div id="xhr200NoErrorSuc4"></div>' +
    '<div id="xhr200NoErrorSuc5"></div>' +
    '<div id="xhr200NoErrorFail"></div>' +
    '<div id="xhr200ErrorSuc"></div>' +
    '<div id="xhr200ErrorFail"></div>' +
    '<div id="xhr0NoErrorSuc"></div>' +
    '<div id="xhr0NoErrorSuc2"></div>' +
    '<div id="xhr0NoErrorSuc3"></div>' +
    '<div id="xhr0NoErrorSuc4"></div>' +
    '<div id="xhr0NoErrorSuc5"></div>' +
    '<div id="xhr0NoErrorFail"></div>' +
    '<div id="xhr0ErrorSuc"></div>' +
    '<div id="xhr0ErrorFail"></div>' +
    '<div id="xhr200NoErrorSucNoCopy"></div>' +
    '<div id="xhr200NoErrorSucNoCopy2"></div>' +
    '<div id="xhr200NoErrorSucNoCopy3"></div>' +
    '<div id="xhr200NoErrorSucNoCopy4"></div>' +
    '<div id="xhr200NoErrorSucNoCopy5"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const xhr200NoErrorSucNode = document.getElementById('xhr200NoErrorSuc');
const xhr200NoErrorSucNode2 = document.getElementById('xhr200NoErrorSuc2');
const xhr200NoErrorSucNode3 = document.getElementById('xhr200NoErrorSuc3');
const xhr200NoErrorSucNode4 = document.getElementById('xhr200NoErrorSuc4');
const xhr200NoErrorSucNode5 = document.getElementById('xhr200NoErrorSuc5');
const xhr200NoErrorFailNode = document.getElementById('xhr200NoErrorFail');
const xhr200ErrorSucNode = document.getElementById('xhr200ErrorSuc');
const xhr200ErrorFailNode = document.getElementById('xhr200ErrorFail');
const xhr0NoErrorSucNode = document.getElementById('xhr0NoErrorSuc');
const xhr0NoErrorSucNode2 = document.getElementById('xhr0NoErrorSuc2');
const xhr0NoErrorSucNode3 = document.getElementById('xhr0NoErrorSuc3');
const xhr0NoErrorSucNode4 = document.getElementById('xhr0NoErrorSuc4');
const xhr0NoErrorSucNode5 = document.getElementById('xhr0NoErrorSuc5');
const xhr0NoErrorFailNode = document.getElementById('xhr0NoErrorFail');
const xhr0ErrorSucNode = document.getElementById('xhr0ErrorSuc');
const xhr0ErrorFailNode = document.getElementById('xhr0ErrorFail');
const xhr200NoErrorSucNoCopyNode = document.getElementById('xhr200NoErrorSucNoCopy');
const xhr200NoErrorSucNoCopyNode2 = document.getElementById('xhr200NoErrorSucNoCopy2');
const xhr200NoErrorSucNoCopyNode3 = document.getElementById('xhr200NoErrorSucNoCopy3');
const xhr200NoErrorSucNoCopyNode4 = document.getElementById('xhr200NoErrorSucNoCopy4');
const xhr200NoErrorSucNoCopyNode5 = document.getElementById('xhr200NoErrorSucNoCopy5');
/** XMLHttpRequest Objects. */
const xhr200NoErrorSuc = {
  status: 200,
  responseData: {
    errors: [],
    success: true,
    copyright: 2018,
  },
};
const xhr200NoErrorSucNoCopy = {
  status: 200,
  responseData: {
    errors: [],
    success: true,
    copyright: '',
  },
};
const xhr200NoErrorFail = {
  status: 200,
  responseData: {
    errors: [],
    success: false,
    copyright: 2018,
  },
};
const xhr200ErrorSuc = {
  status: 200,
  responseData: {
    errors: ['name'],
    success: true,
    copyright: 2018,
  },
};
const xhr200ErrorFail = {
  status: 200,
  responseData: {
    errors: ['name'],
    success: false,
    copyright: 2018,
  },
};
const xhr0NoErrorSuc = {
  status: 0,
  responseData: {
    errors: [],
    success: true,
    copyright: 2018,
  },
};
const xhr0NoErrorFail = {
  status: 0,
  responseData: {
    errors: [],
    success: false,
    copyright: 2018,
  },
};
const xhr0ErrorSuc = {
  status: 0,
  responseData: {
    errors: ['name'],
    success: true,
    copyright: 2018,
  },
};
const xhr0ErrorFail = {
  status: 0,
  responseData: {
    errors: ['name'],
    success: false,
    copyright: 2018,
  },
};
propertiesSetCorrectly = false;

/** Test  ------------------------------ */
module.exports = {
  insertCopyrightTest: function insertCopyrightTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(true, true, true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(false, false, false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(null, null, null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(undefined, undefined, undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(NaN, NaN, NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.INT_POS, input.INT_POS, input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.INT_NEG, input.INT_NEG, input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.STR_LONG, input.STR_LONG, input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY));
    });
const ARRAY_OBJ = [{key: 'value'}, {key: 'value'}, {key: 'value'}];

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(input.OBJ, input.OBJ, input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false,
          prog.insertCopyright(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.insertCopyright(realNode, realNode, realNode));
    });

    it('should return FALSE for (xhr200NoErrorSuc, NULL, 2000) input',
        function() {
      assert.deepStrictEqual(false, prog.insertCopyright(xhr200NoErrorSuc, null, 2000));
    });

    it('should return FALSE for (xhr200NoErrorSuc, realNode, -2000) input',
        function() {
      assert.deepStrictEqual(false, prog.insertCopyright(xhr200NoErrorSuc, realNode, -2000));
    });

    prog.insertCopyright(xhr200NoErrorSuc, xhr200NoErrorSucNode, -0);
    it('should return TRUE for (xhr200NoErrorSuc, xhr200NoErrorSucNode4, -0) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNode.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorSuc, xhr200NoErrorSucNode2, 0);
    it('should return TRUE for (xhr200NoErrorSuc, xhr200NoErrorSucNode2, 0) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNode2.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorSuc, xhr200NoErrorSucNode3, null);
    it('should return TRUE for (xhr200NoErrorSuc, xhr200NoErrorSucNode3, null) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNode3.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorSuc, xhr200NoErrorSucNode4, 2000);
    it('should return TRUE for (xhr200NoErrorSuc, xhr200NoErrorSucNode4, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNode4.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorSuc, xhr200NoErrorSucNode5, 2050);
    it('should return TRUE for (xhr200NoErrorSuc, xhr200NoErrorSucNode4, 2050) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNode5.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorFail, xhr200NoErrorFailNode, 2000);
    it('should return TRUE for (xhr200NoErrorFail, xhr200NoErrorFailNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorFailNode.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200ErrorSuc, xhr200ErrorSucNode, 2000);
    it('should return TRUE for (xhr200ErrorSuc, xhr200ErrorSucNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr200ErrorSucNode.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200ErrorFail, xhr200ErrorFailNode, 2000);
    it('should return TRUE for (xhr200ErrorFail, xhr200ErrorFailNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr200ErrorFailNode.innerHTML === '2018');
    });

    prog.insertCopyright(xhr0NoErrorSuc, xhr0NoErrorSucNode, -0);
    it('should return TRUE for (xhr0NoErrorSuc, xhr0NoErrorSucNode, -0) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorSucNode.innerHTML === '0-2018');
    });

    prog.insertCopyright(xhr0NoErrorSuc, xhr0NoErrorSucNode2, 0);
    it('should return TRUE for (xhr0NoErrorSuc, xhr0NoErrorSucNode2, 0) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorSucNode2.innerHTML === '0-2018');
    });

    prog.insertCopyright(xhr0NoErrorSuc, xhr0NoErrorSucNode3, null);
    it('should return TRUE for (xhr0NoErrorSuc, xhr0NoErrorSucNode3, null) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorSucNode3.innerHTML === '2018');
    });

    prog.insertCopyright(xhr0NoErrorSuc, xhr0NoErrorSucNode4, 2000);
    it('should return TRUE for (xhr0NoErrorSuc, xhr0NoErrorSucNode4, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorSucNode4.innerHTML === '2000-2018');
    });

    prog.insertCopyright(xhr0NoErrorSuc, xhr0NoErrorSucNode5, 2050);
    it('should return TRUE for (xhr0NoErrorSuc, xhr0NoErrorSucNode5, 2050) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorSucNode5.innerHTML === '2018');
    });

    prog.insertCopyright(xhr0NoErrorFail, xhr0NoErrorFailNode, 2000);
    it('should return TRUE for (xhr0NoErrorFail, xhr0NoErrorFailNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr0NoErrorFailNode.innerHTML === '2000-2018');
    });

    prog.insertCopyright(xhr0ErrorSuc, xhr0ErrorSucNode, 2000);
    it('should return TRUE for (xhr0ErrorSuc, xhr0ErrorSucNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr0ErrorSucNode.innerHTML === '2000-2018');
    });

    prog.insertCopyright(xhr0ErrorFail, xhr0ErrorFailNode, 2000);
    it('should return TRUE for (xhr0ErrorFail, xhr0ErrorFailNode, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr0ErrorFailNode.innerHTML === '2000-2018');
    });

    prog.insertCopyright(xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode, -0);
    it('should return TRUE for (xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode, -0) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNoCopyNode.innerHTML === '0-2018');
    });

    prog.insertCopyright(xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode2, 0);
    it('should return TRUE for (xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode2, 0) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNoCopyNode2.innerHTML === '0-2018');
    });

    prog.insertCopyright(xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode3, null);
    it('should return TRUE for (xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode3, null) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNoCopyNode3.innerHTML === '2018');
    });

    prog.insertCopyright(xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode4, 2000);
    it('should return TRUE for (xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode4, 2000) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNoCopyNode4.innerHTML === '2000-2018');
    });

    prog.insertCopyright(xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode5, 2050);
    it('should return TRUE for (xhr200NoErrorSucNoCopy, xhr200NoErrorSucNoCopyNode5, 2050) input',
        function() {
      assert.deepStrictEqual(true, xhr200NoErrorSucNoCopyNode5.innerHTML === '2018');
    });
  }
};
