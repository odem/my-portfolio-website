/** validInteractiveElementObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
/** InteractiveElement Objects */
const interactiveElement = {
  name: 'interactiveElement',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
};
const interactiveElementName = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
};

/** Test  ------------------------------ */
module.exports = {
  validInteractiveElementObjTest: function validInteractiveElementObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj(realNode));
    });

    it('Should return FALSE for {element:input.OBJ} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({element:input.OBJ}));
    });

    it('Should return FALSE for {element:input.ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('Should return FALSE for {element:OBJ, attribute:"data-empty"} input', function() {
      assert.deepStrictEqual(false,
          prog.validInteractiveElementObj({element:input.OBJ, attribute:'data-empty'}));
    });

    it('Should return FALSE for {element:input.OBJ, name: "fake"} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({element:input.OBJ, name:'fake'}));
    });

    it('Should return FALSE for {name: "fake"} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({name:'fake'}));
    });

    it('Should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({element:realNode}));
    });

    it('Should return FALSE for {element:foobarNode, name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validInteractiveElementObj({element:realNode, name:'foobar'}));
    });

    it('Should return TRUE for InteractiveElement input', function() {
      assert.deepStrictEqual(true, prog.validInteractiveElementObj(interactiveElement));
    });

    it('Should return TRUE for interactiveElementName input', function() {
      assert.deepStrictEqual(true, prog.validInteractiveElementObj(interactiveElementName));
    });
  }
};
