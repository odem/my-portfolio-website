/** getFormInputLabel =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar">' +
      '<div id="child"></div>' +
    '</div>' +
    '<form class="js-SxnContact-form" method="post" action="mailer.php">' +
      '<div class="js-SxnContact-formMsg" data-submit="none">' +
        '<p class="js-SxnContact-formMsgText" data-msg="none"></p>' +
      '</div>' +
      '<div class="Form-element">' +
        '<label for="name" class="js-Form-label" data-error="false">Name' +
          '<span class="Form-required">*</span>' +
        '</label>' +
        '<input class="js-Form-input" id="name" name="name" data-error="false" type="text" required>' +
        '<div class="js-Form-inputErrorMsg" data-error="false">A name must be entered.</div>' +
      '</div>' +
    '</form>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const childNode = document.getElementById('child');
const inputError = document.getElementsByClassName('js-Form-label')[0];
const form = document.getElementsByClassName('js-SxnContact-form')[0];

/** Test  ------------------------------ */
module.exports = {
  getFormInputLabelTest: function getFormInputLabelTest(assert, prog, input) {
    it('Should return NULL for true input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(true));
    });

    it('Should return NULL for false input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(false));
    });

    it('Should return NULL for null input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(null));
    });

    it('Should return NULL for undefined input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(undefined));
    });

    it('Should return NULL for NaN input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(NaN));
    });

    it('Should return NULL for INT_POS input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.INT_POS));
    });

    it('Should return NULL for INT_NEG input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.INT_NEG));
    });

    it('Should return NULL for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.INT_ZERO_POS));
    });

    it('Should return NULL for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.INT_ZERO_NEG));
    });

    it('Should return NULL for FLOAT_POS input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.FLOAT_POS));
    });

    it('Should return NULL for FLOAT_NEG input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.FLOAT_NEG));
    });

    it('Should return NULL for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.FLOAT_ZERO_POS));
    });

    it('Should return NULL for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.FLOAT_ZERO_NEG));
    });

    it('Should return NULL for STR_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.STR_EMPTY));
    });

    it('Should return NULL for STR_LONG input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.STR_LONG));
    });

    it('Should return NULL for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_EMPTY));
    });

    it('Should return NULL for ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_NUM));
    });

    it('Should return NULL for ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_STR));
    });

    it('Should return NULL for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_ARRAY_NUM));
    });

    it('Should return NULL for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_ARRAY_STR));
    });

    it('Should return NULL for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return NULL for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ARRAY_OBJ));
    });

    it('Should return NULL for OBJ input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.OBJ));
    });

    it('Should return NULL for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.OBJ_EMPTY));
    });

    it('Should return NULL for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(input.ELEMENT_NODE_FAKE));
    });

    it('Should return null for realNode childNode', function() {
      assert.deepStrictEqual(null, prog.getFormInputLabel(childNode));
    });

    it('Should return inputError for realNode input', function() {
      assert.deepStrictEqual(inputError, prog.getFormInputLabel(realNode));
    });

    it('Should return inputError for realNode input', function() {
      assert.deepStrictEqual(inputError, prog.getFormInputLabel(form));
    });
  }
};
