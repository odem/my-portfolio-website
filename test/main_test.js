  /** Portfolio Site Functions ================================================ */

  var program = {};

  /** Library Functions ============================================= */

  program.isElementNode = function isElementNode(obj) {
    return Boolean(obj) && typeof obj === 'object' && !Array.isArray(obj) &&
        !obj.hasOwnProperty('nodeType') && obj.nodeType === 1;
  };

  program.isNumber = function isNumber(value) {
    return typeof value === 'number' && isFinite(value) &&
        !isNaN(parseFloat(value));
  };

  program.isPositiveNumber = function isPositiveNumber(value) {
    return program.isNumber(value) && value >= 0;
  };

  program.isEmpty = function isEmpty(obj) {
    if (obj) {
      if (Array.isArray(obj)) {
        return obj.length === 0;
      /** Object emptiness based on 'own' properties. */
      } else if (typeof obj === 'object') {
        for (var property in obj) {
          /** obj is not emprty if it has at least 1 'own' property. */
          if (obj.hasOwnProperty(property)) {
            return false;
          }
        }
        /** obj has no 'own' properties. */
        return true;
      /** obj is wrong data type. */
      } else {
        return null;
      }
    /** obj is invalid. */
    } else {
      return null;
    }
  };

  program.toggleDataAttribute = function
      toggleDataAttribute(element, attribute, state1, state2) {
    /**
     * New attribute value.
     * @type {String}
     */
    var newState = '';
    /**
     * Indicates if all arguments are valid.
     * @type {boolean}
     */
    var argsValid = Boolean(element && attribute && state1 && state2) &&
        typeof attribute === 'string' && typeof state1 === 'string' &&
        typeof state2 === 'string' && program.isElementNode(element) &&
        element.hasAttribute(attribute);

    /** Function Start ------------------------------ */
    if (argsValid) {
      newState = element.getAttribute(attribute) === state1 ? state2 : state1;
      element.setAttribute(attribute, newState);
    } else {
      /** TEST PURPOSES. */
      return false;
    }
  };

  program.strEndsWith = function strEndsWith(str, searchStr, length) {
    /**
     * Indicates if str ends with searchStr.
     * @type {boolean}
     */
    var endsWith = false;
    /**
     * Length of str.
     * @type {number}
     */
    var strLength = 0;
    /**
     * Length of searchStr.
     * @type {number}
     */
    var searchStrLength = 0;
    /**
     * Indicates if required string arguments are valid.
     * @type {boolean}
     */
    var strArgsValid = Boolean(str && searchStr) && typeof str === 'string' &&
        typeof searchStr === 'string';

    /** Function Start ------------------------------ */
    if (strArgsValid) {
      strLength = str.length;
      searchStrLength = searchStr.length;

      if (strLength >= searchStrLength) {
        length = (Boolean(length) && program.isPositiveNumber(length) &&
            length < strLength) ? parseInt(length, 10) : strLength;

        if (length >= searchStrLength) {
          endsWith = String.prototype.endsWith ? str.endsWith(searchStr) :
              str.substring((length - searchStrLength), length) === searchStr;
        }
      }
    }

    return endsWith;
  };

  program. modifyClassSuffix = function
      modifyClassSuffix(element, oldSuffix, newSuffix) {
    /**
     * Collection of classes of an element node.
     * @type {Object}
     */
    var classes = null;
    /**
     * Element node classname.
     * @type {String}
     */
    var classname = '';
    /**
     * Substring of an element node's classname.
     * @type {String}
     */
    var classnameSubstr = '';
    /**
     * Indicates if all arguments are valid.
     * @type {boolean}
     */
    var argsValid = Boolean(element && oldSuffix && newSuffix) &&
        program.isElementNode(element) && typeof oldSuffix === 'string' &&
        typeof newSuffix === 'string';

    /** Function Start ------------------------------ */
    if (argsValid) {
      classes = element.classList;

      /** Processes each class of an initializeNode child. */
      for (var k = 0, q = classes.length; k < q; k++ ) {
        classname = classes[k];

        if (program.strEndsWith(classname, oldSuffix)) {
          classnameSubstr = classname.substring(
              0, (classname.length - oldSuffix.length));
          classes.remove(classname);
          classes.add(classnameSubstr + newSuffix);
        }
      }
    } else {
      /** TEST PURPOSES. */
      return false;
    }
  };

  program.getCopyright = function getCopyright(initialYear) {
    /**
     * Holds current year.
     * @type {Date}
     */
    var currentYear = new Date().getFullYear();
    /**
     * String to be returned.
     * @type {String}
     */
    var copyright = '';

    /** Function Start ------------------------------ */
    if (currentYear) {
      if (program.isPositiveNumber(initialYear) &&
          initialYear < currentYear) {
        copyright = initialYear + '-' + currentYear;
      } else {
        copyright = String(currentYear);
      }
    }

    return copyright;
  };

  program.createInteractiveElementObj = function
      createInteractiveElementObj(element) {
    /**
     * Returned InteractiveElement information.
     * @type {Object}
     */
    var obj = {};
    /**
     * Default object name.
     * @type {String}
     */
    var name = 'interactiveElement';
    /**
     * Default element attribute name.
     * @type {String}
     */
    var attr = 'aria-pressed';
    /**
     * Default attribute state that is different from the attrState2 default.
     * @type {String}
     */
    var attrState1 = 'true';
    /**
     * Default attribute state that is different from the attrState1 default.
     * @type {String}
     */
    var attrState2 = 'false';
    /**
     * Indicates if argument is valid.
     * @type {boolean}
     */
    var argValid = Boolean(element) &&
        (program.isElementNode(element) ||
          program.isElementNode(element.element));

    /** Function Start ------------------------------ */
    if (argValid) {
      obj = {
        name: (Boolean(element.name) && typeof element.name === 'string') ?
            element.name : name,
        attribute: (Boolean(element.attribute) &&
            typeof element.attribute === 'string') ? element.attribute : attr,
        attributeState1: (Boolean(element.attributeState1) &&
            typeof element.attributeState1 === 'string') ?
              element.attributeState1 : attrState1,
        attributeState2: (Boolean(element.attributeState2) &&
            typeof element.attributeState2 === 'string') ?
              element.attributeState2 : attrState2,
        element: (element.element || element),
      };
    }

    return obj;
  };

  program.validInteractiveElementObj = function validInteractiveElementObj(obj) {
    /**
     * Indicates if name property value is valid.
     * @type {Boolean}
     */
    var nameValid = false;
    /**
     * Indicates if attribute property value is valid.
     * @type {Boolean}
     */
    var attrValid = false;
    /**
     * Indicates if attributeState1 property value is valid.
     * @type {Boolean}
     */
    var attrState1Valid = false;
    /**
     * Indicates if attributeState2 property value is valid.
     * @type {Boolean}
     */
    var attrState2Valid = false;
    /**
     * Indicates if element property value is valid.
     * @type {Boolean}
     */
    var elementValid = false;
    /**
     * Indicates if obj has ll the InteractiveElement Object properties as
     * 'own' properties.
     * @type {Boolean}
     */
    var hasProperties = Boolean(obj) && obj.hasOwnProperty('name') &&
        obj.hasOwnProperty('element') && obj.hasOwnProperty('attribute') &&
        obj.hasOwnProperty('attributeState1') &&
        obj.hasOwnProperty('attributeState2');

    /** Function Start ------------------------------ */
    if (obj && hasProperties) {
      nameValid = Boolean(obj.name) && typeof obj.name === 'string';
      attrValid = Boolean(obj.attribute) && typeof obj.attribute === 'string';
      attrState1Valid = Boolean(obj.attributeState1) &&
          typeof obj.attributeState1 === 'string';
      attrState2Valid = Boolean(obj.attributeState2) &&
          typeof obj.attributeState2 === 'string';
      elementValid = Boolean(obj.element) && program.isElementNode(obj.element);
    }

    return nameValid && attrValid && attrState1Valid && attrState2Valid &&
        elementValid;
  };

  program.createTextToggleButtonObj = function
      createTextToggleButtonObj(element) {
    /**
     * Returned InteractiveElement information.
     * @type {Object}
     */
    var obj = {};
    /**
     * Object name, initialized to default.
     * @type {String}
     */
    var name = 'textToggleBtn';
    /**
     * Button text when pressed, initialized to default.
     * @type {String}
     */
    var pressedText = '&times;';
    /**
     * Button text when not pressed, initialized to default.
     * @type {String}
     */
    var notPressedText = 'Menu';
    /**
     * Indicates if argument is valid.
     * @type {Boolean}
     */
    var argValid = Boolean(element) &&
        (program.isElementNode(element) ||
          program.isElementNode(element.element));

    /** Function Start ------------------------------ */
    if (argValid) {
      obj = program.createInteractiveElementObj(element);
      obj.pressedText = (Boolean(element.pressedText) &&
          typeof element.pressedText === 'string') ?
            element.pressedText : pressedText;
      obj.notPressedText = (Boolean(element.notPressedText) &&
          typeof element.notPressedText === 'string') ?
            element.notPressedText : notPressedText;

      /** Caller does not specify a name; default name used. */
      if (!element.name) {
        obj.name = name;
      }
    }

    return obj;
  };

  program.validTextToggleBtnObj = function validTextToggleBtnObj(obj) {
    /**
     * Indicates if obj is an InteractiveElement object.
     * @type {Boolean}
     */
    var isInteractiveElementObj = false;
    /**
     * Indicates if pressedText property value is valid.
     * @type {Boolean}
     */
    var pressedTextValid = false;
    /**
     * Indicates if notPressedText property value is valid.
     * @type {Boolean}
     */
    var notPressedTextValid = false;
    /**
     * Indicates if obj contains at least the text properties of the
     * TextToggleBtn object as 'own' properties.
     * @type {Boolean}
     */
    var hasTextProperties = Boolean(obj) && obj.hasOwnProperty('pressedText') &&
        obj.hasOwnProperty('notPressedText');

    /** Function Start ------------------------------ */
    if (obj && hasTextProperties) {
      isInteractiveElementObj = program.validInteractiveElementObj(obj);
      pressedTextValid = Boolean(obj.pressedText) &&
          typeof obj.pressedText === 'string';
      notPressedTextValid = Boolean(obj.notPressedText) &&
          typeof obj.notPressedText === 'string';
    }

    return isInteractiveElementObj && pressedTextValid && notPressedTextValid;
  };

  program.toggleTextToggleButton = function toggleTextToggleButton(btn) {
    /**
     * Indicates if btn is pressed or not.
     * @type {Boolean}
     */
    var btnPressed = false;
    /**
     * Text Displayed when btn is pressed, initialized to default value.
     * @type {String}
     */
    var pressedText = '&times;';
    /**
     * Text Displayed when btn is not pressed, initialized to default value.
     * @type {String}
     */
    var notPressedText = 'Menu';
     /**
     * Indicates if argument is valid.
     * @type {Boolean}
     */
   var argsValid = Boolean(btn) && program.validTextToggleBtnObj(btn) &&
        btn.element.hasAttribute(btn.attribute);

    /** Function Start ------------------------------ */
    if (argsValid) {
      program.toggleDataAttribute(
          btn.element, btn.attribute, btn.attributeState1, btn.attributeState2);

      btnPressed =
          btn.element.getAttribute(btn.attribute) === btn.attributeState1;
      notPressedText =
          btn.notPressedText ? btn.notPressedText : notPressedText;
      pressedText = btn.pressedText ? btn.pressedText : pressedText;
      btn.element.innerHTML = btnPressed ? pressedText : notPressedText;
    }
     else {
    /** TEST PURPOSES. */
    return false;
    }
  };

  program.isEmptyInputData = function isEmptyInputData(input) {
    /**
     * Indicates if argument is valid.
     * @type {Boolean}
     */
    var argValid = Boolean(input) && program.isElementNode(input) &&
        (input.value || input.value === '');

    return argValid ? input.value.trim().length === 0 : null;
  };

  program.isValidInputEmail = function isValidInputEmail(input) {
    return Boolean(input && input.value) && program.isElementNode(input) &&
        input.value.indexOf('@') !== -1;
  };

  program.getAssociatedElementNode = function
      getAssociatedElementNode(classname, element) {
    /**
     * Element node to be returned.
     * @type {Object}
     */
    var node = null;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(element && classname) &&
        program.isElementNode(element) &&
        typeof classname === 'string';

    /** Function Start ------------------------------ */
    if (argsValid) {
      node = (element.getElementsByClassName(classname)[0] ||
          (element.parentNode &&
          element.parentNode.getElementsByClassName(classname)[0]));

      /** The desired element node is not a child or sibling of element. */
      if (!node) {
        node = null;
      }
    }

    return node;
  };


  /** Helper Functions: 2nd Level =========================================== */
  program.validAdblockObj = function validAdblockObj(obj) {
    /**
     * Indicates if element property value is valid.
     * @type {Boolean}
     */
    var elementValid = false;
    /**
     * Indicates if container property value is valid.
     * @type {Boolean}
     */
    var containerValid = false;
    /**
     * If attribute property present, indicates if the property value is valid.
     * @type {Boolean}
     */
    var attrValid = false;
    /**
     * Indicates if obj has the attribute property.
     * @type {Boolean}
     */
    var hasReqProperties = Boolean(obj) && obj.hasOwnProperty('element') &&
        obj.hasOwnProperty('container');

    /** Function Start ------------------------------ */
    if (obj && hasReqProperties) {
      elementValid = Boolean(obj.element) && program.isElementNode(obj.element);
      containerValid = Boolean(obj.container) &&
          program.isElementNode(obj.container);
      attrValid = obj.hasOwnProperty('attribute') &&
        typeof obj.attribute === 'string';
    }

    if (obj && obj.hasOwnProperty('attribute')) {
      return elementValid && containerValid && attrValid;
    } else {
      return elementValid && containerValid;
    }
  };

  program.validFormObj = function validFormObj(obj) {
    /**
     * Indicates if form property value is valid.
     * @type {Boolean}
     */
    var formValid = false;
    /**
     * Indicates if formMsg property value is valid.
     * @type {Boolean}
     */
    var formMsgValid = false;
    /**
     * Indicates if formMsgText property value is valid.
     * @type {Boolean}
     */
    var formMsgTextValid = false;
    /**
     * Indicates if inputs property value is valid.
     * @type {Boolean}
     */
    var inputsValid = false;
    /**
     * Indicates if successMsg property value is valid.
     * @type {Boolean}
     */
    var successMsgValid = false;
    /**
     * Indicates if faliMsg property value is valid.
     * @type {Boolean}
     */
    var failMsgValid = false;
    /**
     * Indicates if obj contains at least all the FormComponent object
     * properties as 'own' properties.
     * @type {Boolean}
     */
    var hasProperties = Boolean(obj) && obj.hasOwnProperty('form') &&
        obj.hasOwnProperty('inputs') && obj.hasOwnProperty('formMsg') &&
        obj.hasOwnProperty('formMsgText') && obj.hasOwnProperty('successMsg') &&
        obj.hasOwnProperty('failMsg');

    /** Function Start ------------------------------ */
    if (obj && hasProperties) {
      formValid = Boolean(obj.form) && program.isElementNode(obj.form);
      formMsgValid = Boolean(obj.formMsg) && program.isElementNode(obj.formMsg);
      formMsgTextValid = Boolean(obj.formMsgText) &&
          program.isElementNode(obj.formMsgText);
      inputsValid = Boolean(obj.inputs) && obj.inputs.length > 0;
      successMsgValid = Boolean(obj.successMsg) &&
          typeof obj.successMsg === 'string';
      failMsgValid = Boolean(obj.failMsg) && typeof obj.failMsg === 'string';
    }

    return formValid && formMsgValid && formMsgTextValid && inputsValid &&
        successMsgValid && failMsgValid;
  };

  program.validPageObj = function validPageObj(obj) {
    /**
     * Indicates if sxns property is valid.
     * @type {Boolean}
     */
    var sxnsValid = false;
    /**
     * Indicates if navSxns property is valid.
     * @type {Boolean}
     */
    var navSxnsValid = false;
    /**
     * Indicates if navLinkAttribute property is valid.
     * @type {Boolean}
     */
    var navLinkAttrValid = false;
    /**
     * Indicates if navPercent property value is valid.
     * @type {Boolean}
     */
    var navPercValid = false;
    /**
     * Indicates if chartsLoaded property value is valid.
     * @type {Boolean}
     */
    var chartsLoadedValid = false;
    /**
     * Indicates if obj contains at least all the PageComponents object
     * properties as 'own' properties.
     * @type {Boolean}
     */
    var hasProperties = Boolean(obj) && obj.hasOwnProperty('sxns') &&
        obj.hasOwnProperty('navSxns') && obj.hasOwnProperty('navPercent') &&
        obj.hasOwnProperty('navLinkAttribute') &&
        obj.hasOwnProperty('chartsLoaded');

    /** Function Start ------------------------------ */
    if (obj && hasProperties) {
      sxnsValid = Boolean(obj.sxns) && typeof obj.sxns === 'object' &&
          !program.isEmpty(obj.sxns);
      navSxnsValid = Boolean(obj.navSxns) && Array.isArray(obj.navSxns) &&
          obj.navSxns.length > 0;
      navLinkAttrValid = Boolean(obj.navLinkAttribute) &&
          typeof obj.navLinkAttribute === 'string';
      chartsLoadedValid = typeof obj.chartsLoaded === 'boolean';
      navPercValid = program.isPositiveNumber(obj.navPercent);
    }

    return sxnsValid && navSxnsValid && navLinkAttrValid && navPercValid &&
        chartsLoadedValid;
  };

  program.validSxnObj = function validSxnObj(obj) {
    /**
     * Indicates if id property value is valid.
     * @type {Boolean}
     */
    var idValid = false;
    /**
     * Indicates if link property value is valid.
     * @type {Boolean}
     */
    var linkValid = false;
    /**
     * Indicates if height property value is valid.
     * @type {Boolean}
     */
    var heightValid = false;
    /**
     * Indicates if yCoordBegin property value is valid.
     * @type {Boolean}
     */
    var yCoordBeginValid = false;
    /**
     * Indicates if yCoorEnd property value is valid.
     * @type {Boolean}
     */
    var yCoordEndValid = false;
    /**
     * Indicates if obj contains at least all the Sxn object properties as
     * 'own' properties.
     * @type {Boolean}
     */
    var hasProperties = Boolean(obj) && obj.hasOwnProperty('id') &&
        obj.hasOwnProperty('link') && obj.hasOwnProperty('height') &&
        obj.hasOwnProperty('yCoordBegin') && obj.hasOwnProperty('yCoordEnd');

    /** Function Start ------------------------------ */
    if (obj && hasProperties) {
      /** id can be falsey, or a non-empty string. */
      idValid = Boolean(!obj.id) || typeof obj.id === 'string';
      /** link can be falsey, or an an element node. */
      linkValid = Boolean(!obj.link) || program.isElementNode(obj.link);
      heightValid = program.isPositiveNumber(obj.height);
      yCoordBeginValid = program.isPositiveNumber(obj.yCoordBegin);
      yCoordEndValid = program.isPositiveNumber(obj.yCoordEnd);
    }

    return idValid && linkValid && heightValid && yCoordBeginValid &&
        yCoordEndValid;
  };

    program.createSxnObjects = function createSxnObjects(sxns, window, document) {
      /**
       * Holds Sxn object information.
       * @type {Object}
       */
      var obj = {};
      /**
       * Holds Sxn objects containing section information.
       * @type {Sxn[]}
       */
      var returnArray = [];
      /**
       * Section id name.
       * @type {String}
       */
      var sxnId = '';
      /**
       * Associated site nav element node link.
       * @type {Object}
       */
      var sxnLink = null;
      /**
       * Holds the height of the section.
       * @type {Number}
       */
      var sxnHeight = 0;
      /**
       * Sxn object containing info about the previous section.
       * @type {Sxn}
       */
      var prevObj = null;
      /**
       * Holds the index of the previous section.
       * @type {Number}
       */
      var prevIndex = 0;
      /**
       * Indicates if first section being processed.
       * @type {Boolean}
       */
      var firstSxn = true;

      /** Function Start ------------------------------ */
      if (sxns && sxns.length) {
        for (var i = 0, n = sxns.length; i < n; i++) {
          if (program.isElementNode(sxns[i])) {
            obj = {
              id: '',
              link: null,
              height: 0,
              yCoordBegin: 0,
              yCoordEnd: 0,
            };

            sxnId = (sxns[i].id || '');
            sxnHeight = parseFloat(
                window.getComputedStyle(sxns[i]).getPropertyValue('height'));
            obj.height = (sxnHeight || 0);

            /** The section contains an id. */
            if (sxnId) {
              obj.id = sxnId;
              sxnLink = document.getElementById(sxnId + 'Link');

              /** The section has an associated site navigation link node. */
              if (sxnLink) {
                obj.link = sxnLink;
              }
            }

            /** The first section does not have a previous section. */
            if (!firstSxn) {
              prevIndex = i - 1;
              prevObj = returnArray[prevIndex];
              /** Adjusted so the coordinate measurement starts at zero. */
              obj.yCoordBegin = prevObj.yCoordBegin + prevObj.height - 1;
            }

            /** Adjusted so the coordinate measurement starts at zero. */
            obj.yCoordEnd = obj.yCoordBegin + obj.height - 1;
            returnArray.push(obj);

            /** After first section is processed, the firstSxn flag negated. */
            if (firstSxn) {
              firstSxn = !firstSxn;
            }
          }
        }
      }

      return returnArray;
    };

  program.getNavSxns = function getNavSxns(sxns, window, document) {
    /**
     * Holds Sxn objects associated with each index of sxns.
     * @type {Sxn[]}
     */
    var sxnsObjArray = [];
    /**
     * Holds only Sxn objects associated with the site nav.
     * @type {Sxn[]}
     */
    var navSxns = [];

    /** Function Start ------------------------------ */
    if (sxns && sxns.length) {
      sxnsObjArray = program.createSxnObjects(sxns, window, document);

      for (var i = 0, n = sxnsObjArray.length; i < n; i++) {
        /** Only page sections associated with site navigation links are added
            to the returned array. */
        if (sxnsObjArray[i].link) {
          navSxns.push(sxnsObjArray[i]);
        }
      }
    }

    return navSxns;
  };

  program.setSiteNavLinks = function
      setSiteNavLinks(sxns, linkAttr, browserPercent, window) {
    /**
     * Number of pixels the document is currently scrolled along the vertical
     * axis with 0.0 indicating top of browser window.
     * @type {Number}
     */
    var yPos = 0.0;
    /**
     * Point at which link state changes.
     * @type {Number}
     */
    var threshold = 0;
    /**
     * Amount of sections in sxns.
     * @type {Number}
     */
    var numSxns = 0;
    /**
     * Keeps track of how many sections processed.
     * @type {Number}
     */
    var count = 0;
    /**
     * Section object.
     * @type {Sxn}
     */
    var sxn = null;
    /**
     * Indicates if section is in browser window view.
     * @type {Boolean}
     */
    var sxnSelected = false;
    /**
     * Precentage of browser window (from top) to trigger change in link state.
     * @type {Number}
     */
    var browserMultiple = 0;

    /** Function Start ------------------------------ */
    if (sxns && Array.isArray(sxns) && linkAttr && typeof linkAttr === 'string' &&
        program.isPositiveNumber(browserPercent)) {
      numSxns = sxns.length;
      yPos = window.pageYOffset;

      /** Caller provided a decimal percent. */
      if (browserPercent <= 1) {
        browserMultiple = browserPercent;
      /** Caller provided a whole number percent. */
      } else if (browserPercent <= 100) {
        browserMultiple = browserPercent / 100;
      /** Caller provided an out-of-range percent value. */
      } else {
        browserMultiple = 1;
      }

      threshold = yPos + (window.innerHeight * browserMultiple);

      /** Processes each section in sxns. */
      while (count < numSxns) {
        sxn = sxns[count++];

        if (sxn && program.validSxnObj(sxn)) {
          sxnSelected =
            sxn.yCoordBegin <= threshold && sxn.yCoordEnd > threshold;
          sxn.link.setAttribute(linkAttr, String(sxnSelected));
        }
      }
    }
    /** TEST PURPOSES. */
     else { return false; }
  };

  program.setSiteNavScrollEvent = function
      setSiteNavScrollEvent(pageItems, action, window) {
    /**
     * Holds add event listener.
     * @type {Object}
     */
    var addEvent = null;
    /**
     * Holds remove event listener.
     * @type {Objectk}
     */
    var removeEvent = null;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = program.validPageObj(pageItems) && action &&
        typeof action === 'string';

    /** Function Start ------------------------------ */
    if (window.addEventListener && argsValid) {
      /** addEvent and removeEvent only strings for TEST PURPOSES. */
      addEvent = "window.addEventListener('scroll', program.setSiteNavLinks.bind(null, pageItems.navSxns, pageItems.navLinkAttribute, pageItems.navPercent), false)";
      removeEvent = "window.removeEventListener('scroll', program.setSiteNavLinks.bind(null, pageItems.navSxns, pageItems.navLinkAttribute, pageItems.navPercent), false)";
      /** "return" is for TEST PURPOSES. */
      return action.toLowerCase() === 'add' ? addEvent : removeEvent;
    }
     else {
      /** TEST PURPOSES. */
      return null;
     }
  };

  program.getFormInputError = function getFormInputError(input) {
    if (input && program.isElementNode(input)) {
      return program.getAssociatedElementNode('js-Form-inputErrorMsg', input);
    } else {
      return null;
    }
  };

  program.getFormInputLabel = function getFormInputLabel(input) {
    if (input && program.isElementNode(input)) {
      return program.getAssociatedElementNode('js-Form-label', input);
    } else {
      return null;
    }
  };

  program.formServerResponse = function formServerResponse(xhr, form) {
    /**
     * Server response data.
     * @type {Object}
     */
    var responseData = null;
    /**
     * Indicates if form submission issue.
     * @type {Boolean}
     */
    var formIssue = false;
    /**
     * Displays 'fail' or 'success' based on form submission status.
     * @type {String}
     */
    var formResponse = '';
    /**
     * Form input element node.
     * @type {Object}
     */
    var formInput = null;
    /**
     * Amount of form inputs.
     * @type {Number}
     */
    var numFormInputs = 0;
    /**
     * Keeps track of which input is being processed.
     * @type {Number}
     */
    var inputIndex = 0;
    /**
     * Form input error message element node.
     * @type {Object}
     */
    var formInputError = null;
    /**
     * Form input label element node.
     * @type {Object}
     */
    var formInputLabel = null;
    /**
     * Name of error.
     * @type {String}
     */
    var error = '';
    /**
     * Amount of errors that occured.
     * @type {Number}
     */
    var numErrors = 0;
    /**
     * Indicates if error is associated with a form input.
     * @type {Boolean}
     */
    var errorFound = false;

    /** Function Start ------------------------------ */
    if (xhr && program.validFormObj(form)) {
      numFormInputs = form.inputs.length;

      if (xhr.status === 200) {
        // responseData = JSON.parse(xhr.responseText);
        /** TEST PURPOSES. */
        responseData = xhr.responseData;
        numErrors = responseData.errors.length;
      }

      /** Form issue happens if server replies with fail signal, or server
          replies with success signal but form action failed. */
      formIssue = (xhr.status != 200 ||
          (xhr.status === 200 && !responseData.success));
      formResponse = formIssue ? 'fail' : 'success';

      form.formMsg.setAttribute('data-submit', formResponse);
      form.formMsgText.setAttribute('data-msg', formResponse);
      form.formMsgText.innerHTML = formIssue ? form.failMsg : form.successMsg;

      /** Clears form inputs upon successful form submission. */
      if (!formIssue) {
        form.form.reset();
      }

      /** Clears attributes on each form input label and error element node.
          This must be done, before (re-)setting the attributes, to clear any
          previously invalid inputs fixed by user. */
      for (var j = 0; j < numFormInputs; j++) {
        formInput = form.inputs[j];
        formInput.setAttribute('data-error', 'false');
        formInputError = program.getFormInputError(formInput);
        formInputLabel = program.getFormInputLabel(formInput);

        if (formInputError) {
          formInputError.setAttribute('data-error', 'false');
        }

        if (formInputLabel) {
          formInputLabel.setAttribute('data-error', 'false');
        }
      }

      /** Sets attributes on form input label and error element nodes
          associated with errors returned from server. */
      if (xhr.status === 200 && !responseData.success && numErrors > 0) {
        for (var i = 0; i < numErrors; i++) {
          error = responseData.errors[i];
          inputIndex = 0;
          errorFound = false;

          /** Looks through form inputs to find the input associated with the
              error returned from the server. */
          while (error && !errorFound && inputIndex < numFormInputs) {
            formInput = form.inputs[inputIndex++];

            /** Input associated with error returned from the server found. */
            if (formInput.classList.contains(error) || formInput.id === error) {
              formInput.setAttribute('data-error', 'true');
              errorFound = true;
              formInputError = program.getFormInputError(formInput);
              formInputLabel = program.getFormInputLabel(formInput);

              if (formInputError) {
                formInputError.setAttribute('data-error', 'true');
              }

              if (formInputLabel) {
                formInputLabel.setAttribute('data-error', 'true');
              }
            }
          }
        }
      }
    }
    /** TEST PURPOSES. */
    return false;
  };


  /** Project Helper Functions: 1st Level =================================== */
    program.setAdblockAttribute = function setAdblockAttribute(obj, window) {
      /**
       * Holds the display property value of the element.
       * @type {String}
       */
      var elementDisplay = '';
      /**
       * Holds the element's attribute name to be set.
       * @type {String}
       */
      var attribute = '';
      /**
       * Indicates if obj has attribute property of AdblockComponents object
       * as an 'own' property, and value is valid.
       * @type {Boolean}
       */
      var hasAttributeProperty = Boolean(obj) &&
          obj.hasOwnProperty('attribute') && typeof obj.attribute === 'string';

      /** Function Start ------------------------------ */
      if (obj && program.validAdblockObj(obj)) {
        elementDisplay =
            window.getComputedStyle(obj.element).getPropertyValue('display');
        attribute = hasAttributeProperty ? obj.attribute : 'data-blocked';

        if (elementDisplay === 'none') {
          obj.container.setAttribute(attribute, 'true');
        } else {
          obj.container.setAttribute(attribute, 'false');
        }
      } else {
        /** TEST PURPOSES. */
        return false;
        // console.log('setAdblockAttribute: one or more invalid parameter(s).');
      }
    };

    program.insertCopyright = function insertCopyright(xhr, element, initialYear) {
      /**
       * Holds server response data.
       * @type {String}
       */
      var responseData = '';

      /** Function Start ------------------------------ */
      if (element && program.isElementNode(element) &&
          (!initialYear || initialYear && program.isPositiveNumber(initialYear))) {
        /** Server responses with an okay signal. */
        if (xhr && xhr.status === 200) {
          // responseData = JSON.parse(xhr.responseText);
          /** TEST PURPOSES. */
          responseData = xhr.responseData;

          if (responseData.copyright != '') {
            /** Uses server's copyright data. */
            element.innerHTML = responseData.copyright;
          } else {
            /** JavaScript computes the copyright year. */
            element.innerHTML = program.getCopyright(initialYear);
          }
        } else {
          /** Server does not response with an okay singal, so JavaScript
              computes the copyright year. */
          element.innerHTML = program.getCopyright(initialYear);
        }
      }
       else {
        /** TEST PURPOSES. */
        return false;
      }
    };

    program.toggleNav = function toggleNav(navObjects) {
      /**
       * Holds an InteractiveElement of TextToggleButton.
       * @type {Object}
       */
      var navObj = null;
      /**
       * Indicats if a valid InteractiveElement object.
       * @type {Boolean}
       */
      var isInteractiveElementObj = false;
      /**
       * Indicates if a valid TextToggleButton object.
       * @type {Boolean}
       */
      var isTextToggleBtnObj = false;

      /** Function Start ------------------------------ */
      /** navObjects is either an object or array. */
      if (navObjects && typeof navObjects === 'object') {
        if (Array.isArray(navObjects)) {
          for (var i = 0, n = navObjects.length; i < n; i++) {
            navObj = navObjects[i];

            if (navObj) {
              isInteractiveElementObj = program.validInteractiveElementObj(navObj);
              isTextToggleBtnObj = program.validTextToggleBtnObj(navObj);

              if (isInteractiveElementObj && !isTextToggleBtnObj) {
                program.toggleDataAttribute(navObj.element, navObj.attribute,
                    navObj.attributeState1, navObj.attributeState2);
              }

              if (isTextToggleBtnObj) {
                program.toggleTextToggleButton(navObj);
              }
            }
          }
        } else {
          for (key in navObjects) {
            navObj = navObjects[key];

            if (navObj) {
              isInteractiveElementObj = program.validInteractiveElementObj(navObj);
              isTextToggleBtnObj = program.validTextToggleBtnObj(navObj);

              if (isInteractiveElementObj && !isTextToggleBtnObj) {
                program.toggleDataAttribute(navObj.element, navObj.attribute,
                    navObj.attributeState1, navObj.attributeState2);
              }

              if (isTextToggleBtnObj) {
                program.toggleTextToggleButton(navObj);
              }
            }
          }
        }
      /** navObjects is not an object or array. */
      } else {
        /** TEST PURPOSES. */
        return false;
        // console.log('toggleNav: invalid parameter.');
      }
    };

    program.loadCharts = function
        loadCharts(charts, pageItems, scrollThreshold, resizeDelay, window, document) {
      /**
       * Indicates if arguments are valid.
       * @type {Boolean}
       */
      var argsValid = Boolean(charts) && charts.length > 0 &&
        program.validPageObj(pageItems) && program.isPositiveNumber(resizeDelay) &&
        program.isPositiveNumber(scrollThreshold);

      /** TEST PURPOSES. */
      return argsValid;
    };

    program.validData = function validData(input) {
      /**
       * Indicates if input is empty.
       * @type {Boolean}
       */
      var inputEmpty = false;
        /**
         * Indicates if data is valid.
         * @type {Boolean}
         */
      var dataValid = false;

      if (input && program.isElementNode(input)) {
        inputEmpty = program.isEmptyInputData(input);

        if (!inputEmpty && input.hasOwnProperty('type') &&
            input.type.toLowerCase() === 'email') {
          dataValid = program.isValidInputEmail(input);
        } else {
          dataValid = (inputEmpty || inputEmpty === null) ? false : true;
        }
      }

      return dataValid;
    };

    program.isSubmitButton = function isSubmitButton(input) {
      return Boolean(input) && program.isElementNode(input) &&
          input.hasAttribute('type') && input.type.toLowerCase() === 'submit';
    };

    program.validateData = function validateData(form) {
      /**
       * Form input element node.
       * @type {Object}
       */
      var input = null;
      /**
       * Form input error message element node.
       * @type {Object}
       */
      var errorMsg = null;
      /**
       * Form label element node.
       * @type {Object}
       */
      var inputLabel = null;
      /**
       * Amount of form inputs.
       * @type {Number}
       */
      var numInputs = 0;
      /**
       * Indicates if data is valid.
       * @type {Boolean}
       */
      var dataValid = false;
      /**
       * Returned validated data; data considered valid unless: invalid form
       * parameter, no form inputs, or invalid data.
       * @type {Object}
       */
      var returnData = {
        valid: true,
        data: {},
      };

      /** Function Start ------------------------------ */
      if (form && program.isElementNode(form) && form.length) {
        numInputs = form.length;

        for (var i = 0; i < numInputs; i++) {
          input = form[i];

          /** Processes all inputs other than submit (button) inputs. */
          if (input && program.isElementNode(input) && input.hasAttribute('name') &&
              !program.isSubmitButton(input)) {
            dataValid = validData(input);
            errorMsg = getFormInputError(input);
            inputLabel = getFormInputLabel(input);
            input.setAttribute('data-error', String(!dataValid));

            if (errorMsg) {
              errorMsg.setAttribute('data-error', String(!dataValid));
            }

            if (inputLabel) {
              inputLabel.setAttribute('data-error', String(!dataValid));
            }

            if (dataValid) {
              returnData.data[input.name] = input.value.trim();
            } else {
              returnData.data[input.name] = '';
              returnData.valid = false;
            }
          }
        }

        /** No form inputs, no data (considered invalid data). */
        if (numInputs === 0) {
          returnData.valid = false;
        }
      /** Invalid form parameter, no data (considered invalid data). */
      } else {
        returnData.valid = false;
      }

      return returnData;
    };

  program.submitContactFormData = function
      submitContactFormData(form, successMsg, failMsg, evt) {
    /**
     * XMLHttpRequest object.
     * @type {Object}
     */
    var xhr = null;
    /**
     * Contains trimmed user input data and properties about the data.
     * @type {Object}
     */
    var data = null;
    /**
     * Contains encodeURIComponent of valid user data.
     * @type {String}
     */
    var encodedData = null;
    /**
     * Form message element node.
     * @type {Object}
     */
    var formMsg = null;
    /**
     * Form message text element node.
     * @type {Object}
     */
    var formMsgText = null;
    /**
     * One or more form input element nodes.
     * @type {Object}
     */
    var formInputs = null;
    /**
     * FormComponent object containing form elements.
     * @type {FormComponents}
     */
    var formElements = {};
    /**
     * Indicates if requred form elements in FormComponents object are valid.
     * @type {Boolean}
     */
    var formElementsValid = false;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(form && successMsg && failMsg) &&
        program.isElementNode(form) && typeof successMsg === 'string' &&
        typeof failMsg === 'string';

    /** Function Start ------------------------------ */
    if (argsValid) {
      // evt.preventDefault();

      // formMsg = form.getElementsByClassName('js-SxnContact-formMsg')[0];
      // formMsgText = form.getElementsByClassName('js-SxnContact-formMsgText')[0];
      // formInputs = form.getElementsByClassName('js-Form-input');
      // formElementsValid = Boolean(formMsg && formMsgText && formInputs);
      // data = program.validateData(form);

      // if (formElementsValid) {
      //   formMsg.setAttribute('data-submit', 'none');
      //   formMsgText.setAttribute('data-msg', 'none');
      // }

      // if (data && data.valid && formElementsValid &&
      //     form.method.toLowerCase() === 'post') {
      //   encodedData = 'formData=' +
      //       encodeURIComponent(JSON.stringify(data.data));

      //   xhr = new XMLHttpRequest();
      //   xhr.open(form.method, form.action, true);
      //   xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
      //   xhr.setRequestHeader('Content-Type',
      //       'application/x-www-form-urlencoded; charset=UTF-8');
      //   xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      //   xhr.send(encodedData);

      //   /** Creates Form object. */
      //   formElements.form = form;
      //   formElements.inputs = formInputs;
      //   formElements.formMsg = formMsg;
      //   formElements.formMsgText = formMsgText;
      //   formElements.successMsg = successMsg;
      //   formElements.failMsg = failMsg;

      //   xhr.addEventListener('loadend',
      //       formServerResponse.bind(null, xhr, formElements), false);
      //   response = program.formServerResponse(xhr, formElements);
      /** No data submitted. */
      // } else {
      //   console.log('submitContactFormData: form element(s) are invalid.');
      // }
      return true;
    /** No data submitted. */
    } else {
      // console.log('submitContactFormData: one or more invalid parameters.');
      return false; // TEST PURPOSES
    }
  };

  module.exports = program;
