/** setSiteNavLinks =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div class="sxn" id="sxn1">' +
      '<div id="sxn1Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn2">' +
      '<div id="sxn2Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn3">' +
      '<div id="sxn3Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn4">' +
      '<div id="sxn4Link"></div>' +
    '</div>' +
  '</body></html>');
/** Virtual DOM window. */
/** window.pageYOffset = 0 */
/** window.innerHeight = 768 */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const sxnNodes = document.getElementsByClassName('sxn');
const sxn1Link = document.getElementById('sxn1Link');
const sxn2Link = document.getElementById('sxn2Link');
const sxn3Link = document.getElementById('sxn3Link');
const sxn4Link = document.getElementById('sxn4Link');
/** @type {Sxn} Nav */
const sxnNavObj1 = {
  id: 'sxnNav1',
  link: sxn1Link,
  height: 0,
  yCoordBegin: 0,
  yCoordEnd: 550,
};
const sxnNavObj2 = {
  id: 'sxnNav2',
  link: sxn2Link,
  height: 0,
  yCoordBegin: 551,
  yCoordEnd: 1200,
};
const sxnNavObj3 = {
  id: 'sxnNav3',
  link: sxn3Link,
  height: 0,
  yCoordBegin: 1201,
  yCoordEnd: 1240,
};
const sxnNavObj4 = {
  id: 'sxnNav4',
  link: sxn4Link,
  height: 0,
  yCoordBegin: 0,
  yCoordEnd: 800,
};
sxnNavObjArray = [sxnNavObj1, sxnNavObj2, sxnNavObj3, sxnNavObj4];
sxnNavObjArrayBadBeg = [[], sxnNavObj1, sxnNavObj2, sxnNavObj3, sxnNavObj4];
sxnNavObjArrayBadMid = [sxnNavObj1, sxnNavObj2, undefined, sxnNavObj3, sxnNavObj4];
sxnNavObjArrayBadLast = [sxnNavObj1, sxnNavObj2, sxnNavObj3, sxnNavObj4, null];
linkAttrsSetCorrectly = false;

/** Test  ------------------------------ */
module.exports = {
  setSiteNavLinksTest: function setSiteNavLinksTest(assert, prog, input) {
    it('Should return false for true input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(true, true, true, window));
    });

    it('Should return false for false input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(false, false, false, window));
    });

    it('Should return false for null input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(null, null, null, window));
    });

    it('Should return false for undefined input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(undefined, undefined, undefined, window));
    });

    it('Should return false for NaN input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(NaN, NaN, NaN, window));
    });

    it('Should return false for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.INT_POS, input.INT_POS, input.INT_POS, window));
    });

    it('Should return false for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.INT_NEG, input.INT_NEG, input.INT_NEG, window));
    });

    it('Should return false for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS, window));
    });

    it('Should return false for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG, window));
    });

    it('Should return false for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS, window));
    });

    it('Should return false for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG, window));
    });

    it('Should return false for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, window));
    });

    it('Should return false for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, window));
    });

    it('Should return false for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY, window));
    });

    it('Should return false for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.STR_LONG, input.STR_LONG, input.STR_LONG, window));
    });

    it('Should return false for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY, window));
    });

    it('Should return false for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM, window));
    });

    it('Should return false for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR, window));
    });

    it('Should return false for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, window));
    });

    it('Should return false for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, window));
    });

    it('Should return false for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, window));
    });

    it('Should return false for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ, window));
    });

    it('Should return false for OBJ input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.OBJ, input.OBJ, input.OBJ, window));
    });

    it('Should return false for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY, window));
    });

    it('Should return false for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, window));
    });

    it('Should return false for realNode input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(realNode, realNode, realNode, window));
    });

    it('should return false for (null, "aria-pressed", .271) input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(null, 'aria-pressed', .271,  window));
    });

    it('should return false for (sxnNavObjArray, "", .271) input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(sxnNavObjArray, '', .271,  window));
    });

    it('should return false for (sxnNavObjArray, "aria-pressed", -.271) input', function() {
      assert.deepStrictEqual(false, prog.setSiteNavLinks(sxnNavObjArray, 'aria-pressed', -.271,  window));
    });

    prog.setSiteNavLinks(sxnNavObjArray, 'aria-pressed', 0,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArray, "aria-pressed", 0) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArray, 'aria-pressed', .271,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArray, "aria-pressed", .271) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', .271,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", .271) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadMid, 'aria-pressed', .271,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadMid, "aria-pressed", .271) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadLast, 'aria-pressed', .271,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadLast, "aria-pressed", .271) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', 1,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", 1) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', 3.14,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", 3.14) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', 42,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", 42) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', 100,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", 100) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });

    prog.setSiteNavLinks(sxnNavObjArrayBadBeg, 'aria-pressed', 1024,  window);
    linkAttrsSetCorrectly =
        sxnNavObj1.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj2.link.getAttribute('aria-pressed') === 'true' &&
        sxnNavObj3.link.getAttribute('aria-pressed') === 'false' &&
        sxnNavObj4.link.getAttribute('aria-pressed') === 'true';

    it('should return true for (sxnNavObjArrayBadBeg, "aria-pressed", 1024) input', function() {
      assert.deepStrictEqual(true, linkAttrsSetCorrectly);
    });
  }
};
