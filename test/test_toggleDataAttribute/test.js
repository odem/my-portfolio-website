/** toggleDataAttribute =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar" data-foo="bar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');

/** Test  ------------------------------ */
module.exports = {
  toggleDataAttributeTest: function toggleDataAttributeTest(assert, prog, input) {
    it('Should return FALSE for (null, "data-wrong", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(null, 'data-foo', 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, null, "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, null, 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", null, "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-foo', null, 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", "bar", null) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-wrong', 'bar', null));
    });

    it('Should return FALSE for (realNode, null, null, null) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, null, null, null));
    });

    it('Should return FALSE for (undefined, "data-wrong", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(undefined, 'data-foo', 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, undefined, "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, undefined, 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", undefined, "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-foo', undefined, 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", "bar", undefined) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-wrong', 'bar', undefined));
    });

    it('Should return FALSE for (realNode, undefined, undefined, undefined) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, undefined, undefined, undefined));
    });

    it('Should return FALSE for (realNode, 42, "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 42, 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", 42, "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-foo', 42, 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", "bar", 42) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-wrong', 'bar', 42));
    });

    it('Should return FALSE for (realNode, 42, 42, 42) input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 42, 42, 42));
    });

    it('Should return FALSE for (OBJ_EMPTY, "data-foo", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(input.OBJ_EMPTY,  'data-foo', 'bar', 'baz'));
    });

    it('Should return FALSE for (OBJ, "data-foo", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(input.OBJ,  'data-foo', 'bar', 'baz'));
    });

    it('Should return FALSE for (ELEMENT_NODE_FAKE, "data-foo", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(input.ELEMENT_NODE_FAKE,  'data-foo', 'bar', 'baz'));
    });

    it('Should return FALSE for (realNode, "data-wrong", "bar", "baz") input', function() {
      assert.deepStrictEqual(false, prog.toggleDataAttribute(realNode, 'data-wrong', 'bar', 'baz'));
    });

    prog.toggleDataAttribute(realNode, 'data-foo', 'bar', 'baz');
    it('Should return TRUE for (realNode, "data-foo", "bar", "baz") input', function() {
      assert.deepStrictEqual(true, realNode.getAttribute('data-foo') === 'baz');
    });
  }
};
