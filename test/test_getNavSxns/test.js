/** getNavSxns =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div class="sxn" id="sxn1">' +
      '<div id="sxn1Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn2">' +
      '<div id="sxn2Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn3">' +
      '<div id="sxn3Link"></div>' +
    '</div>' +
    '<div class="sxn" id="sxn4"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const sxnNodes = document.getElementsByClassName('sxn');
const sxn1Link = document.getElementById('sxn1Link');
const sxn2Link = document.getElementById('sxn2Link');
const sxn3Link = document.getElementById('sxn3Link');
/** Sxn Objects */
const sxn1Obj = {
  id: 'sxn1',
  link: sxn1Link,
  height: 0,
  yCoordBegin: 0,
  yCoordEnd: -1,
};
const sxn2Obj = {
  id: 'sxn2',
  link: sxn2Link,
  height: 0,
  yCoordBegin: -1,
  yCoordEnd: -2,
};
const sxn3Obj = {
  id: 'sxn3',
  link: sxn3Link,
  height: 0,
  yCoordBegin: -2,
  yCoordEnd: -3,
};
const sxnNavArray = [sxn1Obj, sxn2Obj, sxn3Obj];

/** Test  ------------------------------ */
module.exports = {
  getNavSxnsTest: function getNavSxnsTest(assert, prog, input) {
    it('Should return [] for true input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(true, window, document));
    });

    it('Should return [] for false input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(false, window, document));
    });

    it('Should return [] for null input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(null, window, document));
    });

    it('Should return [] for undefined input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(undefined, window, document));
    });

    it('Should return [] for NaN input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(NaN, window, document));
    });

    it('Should return [] for INT_POS input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.INT_POS, window, document));
    });

    it('Should return [] for INT_NEG input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.INT_NEG, window, document));
    });

    it('Should return [] for INT_ZERO_POS input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.INT_ZERO_POS, window, document));
    });

    it('Should return [] for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.INT_ZERO_NEG, window, document));
    });

    it('Should return [] for FLOAT_POS input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.FLOAT_POS, window, document));
    });

    it('Should return [] for FLOAT_NEG input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.FLOAT_NEG, window, document));
    });

    it('Should return [] for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.FLOAT_ZERO_POS, window, document));
    });

    it('Should return [] for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.FLOAT_ZERO_NEG, window, document));
    });

    it('Should return [] for STR_EMPTY input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.STR_EMPTY, window, document));
    });

    it('Should return [] for STR_LONG input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.STR_LONG, window, document));
    });

    it('Should return [] for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_EMPTY, window, document));
    });

    it('Should return [] for ARRAY_NUM input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_NUM, window, document));
    });

    it('Should return [] for ARRAY_STR input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_STR, window, document));
    });

    it('Should return [] for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_ARRAY_NUM, window, document));
    });

    it('Should return [] for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_ARRAY_STR, window, document));
    });

    it('Should return [] for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_OBJ_EMPTY, window, document));
    });

    it('Should return [] for ARRAY_OBJ input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ARRAY_OBJ, window, document));
    });

    it('Should return [] for OBJ input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.OBJ, window, document));
    });

    it('Should return [] for OBJ_EMPTY input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.OBJ_EMPTY, window, document));
    });

    it('Should return [] for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(input.ELEMENT_NODE_FAKE, window, document));
    });

    it('Should return [] for realNode input', function() {
      assert.deepStrictEqual([], prog.getNavSxns(realNode, window, document));
    });

    it('should return [] for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual([], prog.getNavSxns({element:input.OBJ_EMPTY}, window, document));
    });

    it('should return [] for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual([], prog.getNavSxns({element:input.ELEMENT_NODE_FAKE}, window, document));
    });

    it('should return [] for {element:realNode} input', function() {
      assert.deepStrictEqual([], prog.getNavSxns({element:realNode}, window, document));
    });

    it('should return [] for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual([],
          prog.getNavSxns({element:input.OBJ_EMPTY, attribute:'data-empty'}, window, document));
    });

    it('should return [] for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual([],
          prog.getNavSxns({element:input.ELEMENT_NODE_FAKE, name:'fake'}, window, document));
    });

    it('should return [] for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual([],
          prog.getNavSxns({element:realNode, name:'foobar'}, window, document));
    });

    it('should return [] for {name:"foobar"} input', function() {
      assert.deepStrictEqual([], prog.getNavSxns({name:'foobar'}, window, document));
    });

    it('should return [] for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual([], prog.getNavSxns({attribute:'foobar'}, window, document));
    });

    it('should return [] for {element:realNode, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual([],
          prog.getNavSxns({element:realNode, attribute:'foobarNode'}, window, document));
    });

    it('should return sxnNavArray for sxnNodes input', function() {
      assert.deepStrictEqual(sxnNavArray, prog.getNavSxns(sxnNodes, window, document));
    });
  }
};
