/** formServerResponse =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="sibling"></div>' +
    '<form class="js-SxnContact-form" id="form" method="post" action="mailer.php">' +
      '<div class="js-SxnContact-formMsg" data-submit="none">' +
        '<p class="js-SxnContact-formMsgText" data-msg="none"></p>' +
      '</div>' +
      '<div class="Form-element">' +
        '<label for="name" class="js-Form-label" data-error="false">Name' +
        '</label>' +
        '<input class="js-Form-input" id="name" name="name" data-error="false" type="text" required>' +
        '<div class="js-Form-inputErrorMsg" data-error="false">A name must be entered.</div>' +
      '</div>' +
    '</form>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const siblingNode = document.getElementById('sibling');
const formNode = document.getElementsByClassName('js-SxnContact-form')[0];
const inputNodes = document.getElementsByClassName('js-Form-input');
const inputError = document.getElementsByClassName('js-Form-inputErrorMsg')[0];
const inputLabel = document.getElementsByClassName('js-Form-label')[0];
const formMsgNode = document.getElementsByClassName('js-SxnContact-formMsg')[0];
const formMsgTextNode = document.getElementsByClassName('js-SxnContact-formMsgText')[0];
const sucMsg = 'success message.';
const failMsg = 'failure message.'
/** FormComponents Objects */
const formObjReal = {
  form: formNode,
  inputs: inputNodes,
  formMsg: formMsgNode,
  formMsgText: formMsgTextNode,
  successMsg: sucMsg,
  failMsg: failMsg,
};
const formObjBad = {
  form: realNode,
  inputs: [1,2,2,5],
  formMsg: 'childNode',
  formMsgText: siblingNode,
  successMsg: 'success',
  failMsg: 'fail',
};
/** XMLHttpRequest Objects. */
const xhr200NoErrorSuc = {
  status: 200,
  responseData: {
    errors: [],
    success: true,
    copyright: 2018,
  },
};
const xhr200NoErrorFail = {
  status: 200,
  responseData: {
    errors: [],
    success: false,
    copyright: 2018,
  },
};
const xhr200ErrorSuc = {
  status: 200,
  responseData: {
    errors: ['name'],
    success: true,
    copyright: 2018,
  },
};
const xhr200ErrorFail = {
  status: 200,
  responseData: {
    errors: ['name'],
    success: false,
    copyright: 2018,
  },
};
const xhr0NoErrorSuc = {
  status: 0,
  responseData: {
    errors: [],
    success: true,
    copyright: 2018,
  },
};
const xhr0NoErrorFail = {
  status: 0,
  responseData: {
    errors: [],
    success: false,
    copyright: 2018,
  },
};
const xhr0ErrorSuc = {
  status: 0,
  responseData: {
    errors: ['name'],
    success: true,
    copyright: 2018,
  },
};
const xhr0ErrorFail = {
  status: 0,
  responseData: {
    errors: ['name'],
    success: false,
    copyright: 2018,
  },
};
propertiesSetCorrectly = false;

/** Test  ------------------------------ */
module.exports = {
  formServerResponseTest: function formServerResponseTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(true, true, window, document));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(false, false, window, document));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(null, null, window, document));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(undefined, undefined, window, document));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(NaN, NaN, window, document));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.INT_POS, input.INT_POS, window, document));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.INT_NEG, input.INT_NEG, window, document));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.INT_ZERO_POS, input.INT_ZERO_POS, window, document));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.INT_ZERO_NEG, input.INT_ZERO_NEG, window, document));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.FLOAT_POS, input.FLOAT_POS, window, document));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.FLOAT_NEG, input.FLOAT_NEG, window, document));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, window, document));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, window, document));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.STR_EMPTY, input.STR_EMPTY, window, document));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.STR_LONG, input.STR_LONG, window, document));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_EMPTY, input.ARRAY_EMPTY, window, document));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_NUM, input.ARRAY_NUM, window, document));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_STR, input.ARRAY_STR, window, document));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, window, document));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, window, document));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, window, document));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ARRAY_OBJ, input.ARRAY_OBJ, window, document));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.OBJ, input.OBJ, window, document));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.OBJ_EMPTY, input.OBJ_EMPTY, window, document));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, window, document));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(realNode, realNode, window, document));
    });

    it('Should return FALSE for (xhr200NoErrorSuc, formObjBad) input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(xhr200NoErrorSuc, formObjBad, window, document));
    });

    it('Should return FALSE for (null, formObjReal) input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(null, formObjReal, window, document));
    });

    it('Should return FALSE for (undefined, formObjReal) input', function() {
      assert.deepStrictEqual(false, prog.formServerResponse(undefined, formObjReal, window, document));
    });

    prog.formServerResponse(xhr200NoErrorSuc, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'success' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'success' &&
        formObjReal.formMsgText.innerHTML === formObjReal.successMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr200NoErrorSuc, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr200NoErrorFail, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr200NoErrorFail, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr200ErrorSuc, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'successMsg' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'successMsg' &&
        formObjReal.formMsgText.innerHTML === formObjReal.successMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr200ErrorSuc, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr200ErrorFail, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'true' &&
        inputError.getAttribute('data-error') === 'true' &&
        inputLabel.getAttribute('data-error') === 'true';

    it('Should return TRUE for (xhr200ErrorFail, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr0NoErrorSuc, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr0NoErrorSuc, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr0NoErrorFail, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr0NoErrorFail, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr0ErrorSuc, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr0ErrorSuc, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.formServerResponse(xhr0ErrorFail, formObjReal, window, document);
    propertiesSetCorrectly =
        formObjReal.formMsg.getAttribute('data-submit') === 'fail' &&
        formObjReal.formMsgText.getAttribute('data-msg') === 'fail' &&
        formObjReal.formMsgText.innerHTML === formObjReal.failMsg &&
        formObjReal.inputs[0].getAttribute('data-error') === 'false' &&
        inputError.getAttribute('data-error') === 'false' &&
        inputLabel.getAttribute('data-error') === 'false';

    it('Should return TRUE for (xhr0ErrorFail, formObjReal) input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

  }
};
