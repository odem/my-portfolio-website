/** isSubmitButton =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<form class="js-SxnContact-form" method="post" action="mailer.php">' +
      '<div class="js-SxnContact-formMsg" data-submit="none">' +
        '<p class="js-SxnContact-formMsgText" data-msg="none"></p>' +
      '</div>' +
      '<div>' +
        '<label for="name" class="js-Form-label" data-error="false">Name' +
        '</label>' +
        '<input class="js-Form-input" id="emptyInput" name="" data-error="false" type="text" required>' +
        '<input class="js-Form-input" id="name" name="name" data-error="false" type="text" required>' +
        '<input class="js-Form-input" id="email" name="email" data-error="false" type="text" required>' +
        '<input class="js-Form-input" id="submit" name="submit" data-error="false" type="submit" required>' +
        '<div class="js-Form-inputErrorMsg" data-error="false">A name must be entered.</div>' +
      '</div>' +
    '</form>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const emptyInputNode = document.getElementById('emptyInput');
const nameInputNode = document.getElementById('name');
const emailInputNode = document.getElementById('email');
const submitNode = document.getElementById('submit');

/** Test  ------------------------------ */
module.exports = {
  isSubmitButtonTest: function isSubmitButtonTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(realNode));
    });

    it('should return FALSE for {element:input.OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton({element:input.OBJ_EMPTY}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton({element:realNode}));
    });

    it('should return FALSE for {element:input.OBJ_EMPTY, type:"submit"}', function() {
      assert.deepStrictEqual(false,
          prog.isSubmitButton({element:input.OBJ_EMPTY, type:'submit'}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.isSubmitButton({element:input.ELEMENT_NODE_FAKE, name:'fake'}));
    });

    it('should return FALSE for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.isSubmitButton({element:realNode, name:'foobar'}));
    });

    it('should return FALSE for {type:"submit"} input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton({type:'submit'}));
    });

    it('should return FALSE for emptyInputNode input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(emptyInputNode));
    });

    it('should return FALSE for nameInputNode input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(nameInputNode));
    });

    it('should return FALSE for emailInputNode input', function() {
      assert.deepStrictEqual(false, prog.isSubmitButton(emailInputNode));
    });

    it('should return TRUE for submitNode input', function() {
      assert.deepStrictEqual(true, prog.isSubmitButton(submitNode));
    });
  }
};
