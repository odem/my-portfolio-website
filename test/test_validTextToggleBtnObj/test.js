/** validTextToggleBtnObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="ariaPressedFalse" aria-pressed="false"></div>' +
    '<div id="ariaPressedTrue" aria-pressed="true"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const ariaPressedFalseNode = document.getElementById('ariaPressedFalse');
const ariaPressedTrueNode = document.getElementById('ariaPressedTrue');
/** TextToggleButton Objects */
const textToggleButton = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: '&times;',
  notPressedText: 'Menu',
};
const textToggleButtonName = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: '&times;',
  notPressedText: 'Menu',
};
const textToggleButtonText = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: 'foo',
  notPressedText: 'bar',
};
const textToggleButtonNameText = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  notPressedText: 'bar',
  pressedText: 'foo',
};
const textToggleButtonFalse = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: ariaPressedFalseNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const textToggleButtonTrue = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: ariaPressedTrueNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};

/** Test  ------------------------------ */
module.exports = {
  validTextToggleBtnObjTest: function validTextToggleBtnObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj(realNode));
    });

    it('should return FALSE for {element:input.OBJ} input', function() {
      assert.deepStrictEqual(false, prog.validTextToggleBtnObj({element:input.OBJ}));
    });

    it('should return TRUE for textToggleButton input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButton));
    });

    it('should return TRUE for textToggleButtonText input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButtonText));
    });

    it('should return TRUE for textToggleButtonName input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButtonName));
    });

    it('should return TRUE for textToggleButtonNameText input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButtonNameText));
    });

    it('should return TRUE for textToggleButtonFalse input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButtonFalse));
    });

    it('should return TRUE for textToggleButtonTrue input', function() {
      assert.deepStrictEqual(true, prog.validTextToggleBtnObj(textToggleButtonTrue));
    });
  }
};
