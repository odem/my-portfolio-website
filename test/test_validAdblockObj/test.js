/** validAdblockObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar">' +
      '<div id="child"></div>' +
    '</div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const childNode = document.getElementById('child');
/** AdblockComponents Objects */
const adblockObj = {
  element: realNode,
  container: childNode,
};
const adblockObjBad = {
  element: 'foobarNode',
  container: childNode,
};
const adblockObjAttr = {
  element: childNode,
  container: realNode,
  attribue: 'foobar',
};
const adblockObjAttrBad = {
  element: childNode,
  container: realNode,
  attribute: realNode,
};

/** Test  ------------------------------ */
module.exports = {
  validAdblockObjTest: function validAdblockObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(realNode));
    });

    it('should return FALSE for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({element:input.OBJ_EMPTY}));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({element:realNode}));
    });

    it('should return FALSE for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual(false,
          prog.validAdblockObj({element:input.OBJ_EMPTY, attribute:'data-empty'}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.validAdblockObj({element:input.ELEMENT_NODE_FAKE, name:'fake'}));
    });

    it('should return FALSE for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.validAdblockObj({element:realNode, name:'foobar'}));
    });

    it('should return FALSE for {name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({name:'foobar'}));
    });

    it('should return FALSE for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({attribute:'foobar'}));
    });

    it('should return FALSE for {container:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj({container:realNode}));
    });

    it('should return FALSE for {element:realNode, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual(false,
          prog.validAdblockObj({element:realNode, attribute:'foobarNode'}));
    });

    it('should return FALSE for adblockObjBad input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(adblockObjBad));
    });

    it('should return TRUE for adblockObjAttr input', function() {
      assert.deepStrictEqual(true, prog.validAdblockObj(adblockObjAttr));
    });

    it('should return FALSE for adblockObjAttrBad input', function() {
      assert.deepStrictEqual(false, prog.validAdblockObj(adblockObjAttrBad));
    });

    it('should return TRUE for adblockObj input', function() {
      assert.deepStrictEqual(true, prog.validAdblockObj(adblockObj));
    });
  }
};
