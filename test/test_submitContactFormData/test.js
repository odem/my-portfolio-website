/** submitContactFormData =========================================================== */
/** ONLY tested if arguments are valid. Not included in production test. */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');

/** Test  ------------------------------ */
module.exports = {
  submitContactFormDataTest: function submitContactFormDataTest(assert, prog, input) {
    it('should return FALSE for Boolean true input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(true, true, true, true));
    });

    it('should return FALSE for Boolean false input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(false, false, false, false));
    });

    it('should return FALSE for NULL input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(null, null, null, null));
    });

    it('should return FALSE for UNDEEFINED input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(undefined, undefined, undefined, undefined));
    });

    it('should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(NaN, NaN, NaN, NaN));
    });

    it('should return FALSE for input.INT_POS input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.INT_POS, input.INT_POS, input.INT_POS, input.INT_POS));
    });

    it('should return FALSE for input.FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS));
    });

    it('should return FALSE for positive zero integer input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS));
    });

    it('should return FALSE for positive zero float input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS));
    });

    it('should return FALSE for negative zero integer input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG));
    });

    it('should return FALSE for negative zero float input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG));
    });

    it('should return FALSE for negative integer input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.INT_NEG, input.INT_NEG, input.INT_NEG, input.INT_NEG));
    });

    it('should return FALSE for negative float input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG));
    });

    it('should return FALSE for empty string input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY));
    });

    it('should return FALSE for non-empty string input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.STR_LONG, input.STR_LONG, input.STR_LONG, input.STR_LONG));
    });

    it('should return FALSE for empty input.ARRAY input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY));
    });

    it('should return FALSE for input.ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM));
    });

    it('should return FALSE for input.ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR));
    });

    it('should return FALSE for input.ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM));
    });

    it('should return FALSE for input.ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR));
    });

    it('should return FALSE for input.ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY));
    });

    it('should return FALSE for input.ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ));
    });

    it('should return FALSE for empty object input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY));
    });

    it('should return FALSE for fake element node input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.OBJ, input.OBJ, input.OBJ, input.OBJ));
    });

    it('should return FALSE for empty fake element node input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE));
    });

    it('should return FALSE for real element node input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(realNode, realNode, realNode, realNode));
    });

    it('should return FALSE for ({element:input.ELEMENT_NODE_FAKE}, {success:input.STR_LONG}, {fail:input.STR_LONG}, {event:true}) input', function() {
      assert.deepStrictEqual(false,
          prog.submitContactFormData(
            {element: input.ELEMENT_NODE_FAKE},
            {success: input.STR_LONG},
            {fail: input.STR_LONG},
            {event: true}));
    });

    it('should return FALSE for (null, input.STR_LONG, input.STR_LONG, true) input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(null, input.STR_LONG, input.STR_LONG, true));
    });

    it('should return FALSE for (realNode, "", input.STR_LONG, true) input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(realNode, '', input.STR_LONG, true));
    });

    it('should return FALSE for (realNode, input.STR_LONG, "", true) input', function() {
      assert.deepStrictEqual(false, prog.submitContactFormData(realNode, input.STR_LONG, '', true));
    });

    it('should return TRUE for (realNode, input.STR_LONG, input.STR_LONG, true) input', function() {
      assert.deepStrictEqual(true, prog.submitContactFormData(realNode, input.STR_LONG, input.STR_LONG, true));
    });
  }
};
