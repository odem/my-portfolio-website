/** validSxnObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
/** Sxn Objects */
const sxnObjNulls = {
  id: '',
  link: null,
  height: 1130,
  yCoordBegin: 2.71,
  yCoordEnd: 3.14,
};
const sxnObjFull = {
  id: 'idname',
  link: realNode,
  height: 1130,
  yCoordBegin: 2.71,
  yCoordEnd: 3.14,
};
const sxnObjBad = {
  id: 'idname',
  link: realNode,
  height: -1130,
  yCoordBegin: 2.71,
  yCoordEnd: null,
};

/** Test  ------------------------------ */
module.exports = {
  validSxnObjTest: function validSxnObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(realNode));
    });

    it('should return FALSE for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj({element:input.OBJ_EMPTY}));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj({element:realNode}));
    });

    it('should return FALSE for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual(false,
          prog.validSxnObj({element:input.OBJ_EMPTY, attribute:'data-empty'}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.validSxnObj({element:input.ELEMENT_NODE_FAKE, name:'fake'}));
    });

    it('should return FALSE for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.validSxnObj({element:realNode, name:'foobar'}));
    });

    it('should return FALSE for {name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj({name:'foobar'}));
    });

    it('should return FALSE for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj({attribute:'foobar'}));
    });

    it('should return FALSE for {element:realNode, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual(false,
          prog.validSxnObj({element:realNode, attribute:'foobarNode'}));
    });

    it('should return FALSE for sxnObjBad input', function() {
      assert.deepStrictEqual(false, prog.validSxnObj(sxnObjBad));
    });

    it('should return TRUE for sxnObjNulls input', function() {
      assert.deepStrictEqual(true, prog.validSxnObj(sxnObjNulls));
    });

    it('should return TRUE for sxnObjFull input', function() {
      assert.deepStrictEqual(true, prog.validSxnObj(sxnObjFull));
    });
  }
};
