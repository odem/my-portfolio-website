/** isEmpty =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');

/** Test  ------------------------------ */
module.exports = {
  isEmptyTest: function isEmptyTest(assert, prog, input) {
    it('Should return NULL for true input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(true));
    });

    it('Should return NULL for false input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(false));
    });

    it('Should return NULL for null input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(null));
    });

    it('Should return NULL for undefined input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(undefined));
    });

    it('Should return NULL for NaN input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(NaN));
    });

    it('Should return NULL for INT_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.INT_POS));
    });

    it('Should return NULL for INT_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.INT_NEG));
    });

    it('Should return NULL for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.INT_ZERO_POS));
    });

    it('Should return NULL for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.INT_ZERO_NEG));
    });

    it('Should return NULL for FLOAT_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.FLOAT_POS));
    });

    it('Should return NULL for FLOAT_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.FLOAT_NEG));
    });

    it('Should return NULL for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.FLOAT_ZERO_POS));
    });

    it('Should return NULL for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.FLOAT_ZERO_NEG));
    });

    it('Should return NULL for STR_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.STR_EMPTY));
    });

    it('Should return NULL for STR_LONG input', function() {
      assert.deepStrictEqual(null, prog.isEmpty(input.STR_LONG));
    });

    it('Should return TRUE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(true, prog.isEmpty(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.OBJ));
    });

    it('Should return TRUE for OBJ_EMPTY object input', function() {
      assert.deepStrictEqual(true, prog.isEmpty(input.OBJ_EMPTY));
    });

    it('Should return NULL for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.isEmpty(input.ELEMENT_NODE_FAKE));
    });

    it('Should return TRUE for realNode input', function() {
      assert.deepStrictEqual(true, prog.isEmpty(realNode));
    });
  }
};
