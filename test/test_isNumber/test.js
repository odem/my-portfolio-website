/** isNumber =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');

/** Test  ------------------------------ */
module.exports = {
  isNumberTest: function isNumberTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.isNumber(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.isNumber(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.isNumber(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.isNumber(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.isNumber(NaN));
    });

    it('Should return TRUE for INT_POS input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.INT_POS));
    });

    it('Should return TRUE for INT_NEG input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.INT_NEG));
    });

    it('Should return TRUE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.INT_ZERO_POS));
    });

    it('Should return TRUE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.INT_ZERO_NEG));
    });

    it('Should return TRUE for FLOAT_POS input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.FLOAT_POS));
    });

    it('Should return TRUE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.FLOAT_NEG));
    });

    it('Should return TRUE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.FLOAT_ZERO_POS));
    });

    it('Should return TRUE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(true, prog.isNumber(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.isNumber(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.isNumber(realNode));
    });
  }
};
