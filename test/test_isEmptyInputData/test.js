/** isEmptyInputData =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<input id="inputEmpty" value="">' +
    '<input id="inputName" value="name">' +
    '<input id="inputEmail" value="foo@bar.com">' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const emptyInputNode = document.getElementById('inputEmpty');
const nameInputNode = document.getElementById('inputName');
const emailInputNode = document.getElementById('inputEmail');

/** Test  ------------------------------ */
module.exports = {
  isEmptyInputDataTest: function isEmptyInputDataTest(assert, prog, input) {
    it('Should return NULL for true input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(true));
    });

    it('Should return NULL for false input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(false));
    });

    it('Should return NULL for null input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(null));
    });

    it('Should return NULL for undefined input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(undefined));
    });

    it('Should return NULL for NaN input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(NaN));
    });

    it('Should return NULL for INT_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.INT_POS));
    });

    it('Should return NULL for INT_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.INT_NEG));
    });

    it('Should return NULL for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.INT_ZERO_POS));
    });

    it('Should return NULL for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.INT_ZERO_NEG));
    });

    it('Should return NULL for FLOAT_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.FLOAT_POS));
    });

    it('Should return NULL for FLOAT_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.FLOAT_NEG));
    });

    it('Should return NULL for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.FLOAT_ZERO_POS));
    });

    it('Should return NULL for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.FLOAT_ZERO_NEG));
    });

    it('Should return NULL for STR_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.STR_EMPTY));
    });

    it('Should return NULL for STR_LONG input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.STR_LONG));
    });

    it('Should return NULL for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_EMPTY));
    });

    it('Should return NULL for ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_NUM));
    });

    it('Should return NULL for ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_STR));
    });

    it('Should return NULL for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_ARRAY_NUM));
    });

    it('Should return NULL for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_ARRAY_STR));
    });

    it('Should return NULL for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return NULL for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ARRAY_OBJ));
    });

    it('Should return NULL for OBJ input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.OBJ));
    });

    it('Should return NULL for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.OBJ_EMPTY));
    });

    it('Should return NULL for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(input.ELEMENT_NODE_FAKE));
    });

    it('should return NULL for realNode input', function() {
      assert.deepStrictEqual(null, prog.isEmptyInputData(realNode));
    });

    it('should return FALSE for nameInputNode input', function() {
      assert.deepStrictEqual(false, prog.isEmptyInputData(nameInputNode));
    });

    it('should return FALSE for emailInputNode input', function() {
      assert.deepStrictEqual(false, prog.isEmptyInputData(emailInputNode));
    });

    it('should return TRUE for emptyInputNode input', function() {
      assert.deepStrictEqual(true, prog.isEmptyInputData(emptyInputNode));
    });
  }
};
