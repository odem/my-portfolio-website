/** getCopyright =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');

/** Test  ------------------------------ */
module.exports = {
  getCopyrightTest: function getCopyrightTest(assert, prog, input) {
    it('Should return YEAR_CURRENT for true input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(true));
    });

    it('Should return YEAR_CURRENT for false input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(false));
    });

    it('Should return YEAR_CURRENT for null input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(null));
    });

    it('Should return YEAR_CURRENT for undefined input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(undefined));
    });

    it('Should return YEAR_CURRENT for NaN input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(NaN));
    });

    it('Should return YEAR_CURRENT for YEAR_CURRENT input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.YEAR_CURRENT));
    });

    it('Should return YEAR_CURRENT for YEAR_GREATER_THAN_CURRENT input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.YEAR_GREATER_THAN_CURRENT));
    });

    it('Should return YEAR_LESS_THAN_CURRENT-YEAR_CURRENT for YEAR_LESS_THAN_CURRENT input', function() {
      assert.deepStrictEqual(String(input.YEAR_LESS_THAN_CURRENT) + '-' + String(input.YEAR_CURRENT),
          prog.getCopyright(input.YEAR_LESS_THAN_CURRENT));
    });

    it('Should return 0-YEAR_CURRENT for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(String(input.INT_ZERO_POS) + '-' + String(input.YEAR_CURRENT),
          prog.getCopyright(input.INT_ZERO_POS));
    });

    it('Should return 0.0-YEAR_CURRENT for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(String(input.FLOAT_ZERO_POS) + '-' + String(input.YEAR_CURRENT),
          prog.getCopyright(input.FLOAT_ZERO_POS));
    });

    it('Should return 0-YEAR_CURRENT for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(String(input.INT_ZERO_NEG) + '-' + String(input.YEAR_CURRENT),
          prog.getCopyright(input.INT_ZERO_NEG));
    });

    it('Should return 0.0-YEAR_CURRENT for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(String(input.FLOAT_ZERO_NEG) + '-' + String(input.YEAR_CURRENT),
          prog.getCopyright(input.FLOAT_ZERO_NEG));
    });

    it('Should return YEAR_CURRENT for INT_NEG input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.INT_NEG));
    });

    it('Should return YEAR_CURRENT for FLOAT_NEG input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.FLOAT_NEG));
    });

    it('Should return YEAR_CURRENT for STR_EMPTY input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.STR_EMPTY));
    });

    it('Should return YEAR_CURRENT for STR_LONG input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.STR_LONG));
    });

    it('Should return YEAR_CURRENT for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_EMPTY));
    });

    it('Should return YEAR_CURRENT for ARRAY_NUM input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_NUM));
    });

    it('Should return YEAR_CURRENT for ARRAY_STR input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_STR));
    });

    it('Should return YEAR_CURRENT for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_ARRAY_NUM));
    });

    it('Should return YEAR_CURRENT for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return YEAR_CURRENT for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ARRAY_OBJ));
    });

    it('Should return YEAR_CURRENT for OBJ input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.OBJ));
    });

    it('Should return YEAR_CURRENT for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.OBJ_EMPTY));
    });

    it('Should return YEAR_CURRENT for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(input.ELEMENT_NODE_FAKE));
    });

    it('Should return YEAR_CURRENT for realNode input', function() {
      assert.deepStrictEqual(String(input.YEAR_CURRENT), prog.getCopyright(realNode));
    });
  }
};
