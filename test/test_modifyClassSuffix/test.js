/** modifyClassSuffix =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="nojs" class="nav--noJs"></div>' +
    '<div id="nojs2" class="nav--noJs"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const noJsNode = document.getElementById('nojs');
const noJsNode2 = document.getElementById('nojs2');

/** Test  ------------------------------ */
module.exports = {
  modifyClassSuffixTest: function modifyClassSuffixTest(assert, prog, input) {
    prog.modifyClassSuffix(true, 'noJs', 'js');
    it('should return FALSE for (true, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (true.class === 'js'));
    });

    prog.modifyClassSuffix(false, 'noJs', 'js');
    it('should return FALSE for (false, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (false.class === 'js'));
    });

    prog.modifyClassSuffix(null, 'noJs', 'js');
    it('should return FALSE for (null, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (null === 'js'));
    });

    prog.modifyClassSuffix(undefined, 'noJs', 'js');
    it('should return FALSE for (undefined, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (undefined === 'js'));
    });

    prog.modifyClassSuffix(NaN, 'noJs', 'js');
    it('should return FALSE for (NaN, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (NaN.class === 'js'));
    });

    prog.modifyClassSuffix(input.INT_POS, 'noJs', 'js');
    it('should return FALSE for (input.INT_POS, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.INT_POS.class === 'js'));
    });

    prog.modifyClassSuffix(input.INT_ZERO_POS, 'noJs', 'js');
    it('should return FALSE for (input.INT_ZERO_POS, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.INT_ZERO_POS.class === 'js'));
    });

    prog.modifyClassSuffix(input.INT_NEG, 'noJs', 'js');
    it('should return FALSE for (input.INT_NEG, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.INT_NEG.class === 'js'));
    });

    prog.modifyClassSuffix(input.INT_ZERO_NEG, 'noJs', 'js');
    it('should return FALSE for (input.INT_ZERO_NEG, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.INT_ZERO_NEG.class === 'js'));
    });

    prog.modifyClassSuffix(input.FLOAT_POS, 'noJs', 'js');
    it('should return FALSE for (input.FLOAT_POS, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.FLOAT_POS.class === 'js'));
    });

    prog.modifyClassSuffix(input.FLOAT_ZERO_POS, 'noJs', 'js');
    it('should return FALSE for (input.FLOAT_ZERO_POS, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.FLOAT_ZERO_POS.class === 'js'));
    });

    prog.modifyClassSuffix(input.FLOAT_NEG, 'noJs', 'js');
    it('should return FALSE for (input.FLOAT_NEG, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.FLOAT_NEG.class === 'js'));
    });

    prog.modifyClassSuffix(input.FLOAT_ZERO_NEG, 'noJs', 'js');
    it('should return FALSE for (input.FLOAT_ZERO_NEG, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.FLOAT_ZERO_NEG.class === 'js'));
    });

    prog.modifyClassSuffix(input.OBJ_EMPTY, 'noJs', 'js');
    it('should return FALSE for (input.OBJ_EMPTY, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.OBJ_EMPTY.class === 'js'));
    });

    prog.modifyClassSuffix(input.OBJ, 'noJs', 'js');
    it('should return FALSE for (input.OBJ, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.OBJ.class === 'js'));
    });

    prog.modifyClassSuffix(input.ELEMENT_NODE_FAKE, 'noJs', 'js');
    it('should return FALSE for (input.ELEMENT_NODE_FAKE, "noJs", "js")', function() {
      assert.deepStrictEqual(false, (input.ELEMENT_NODE_FAKE.class === 'js'));
    });

    prog.modifyClassSuffix(realNode, '', 'js');
    it('should return FALSE for (realNode, "", "js")', function() {
      assert.deepStrictEqual(false,
          (realNode.classList.contains('js') && !realNode.classList.contains('')));
    });

    prog.modifyClassSuffix(realNode, 'noJs', '');
    it('should return FALSE for (realNode, "noJs", "")', function() {
      assert.deepStrictEqual(false,
          (realNode.classList.contains('') && !realNode.classList.contains('nav--noJs')));
    });

    prog.modifyClassSuffix(realNode, '', '');
    it('should return FALSE for (realNode, "", "")', function() {
      assert.deepStrictEqual(false,
          (realNode.classList.contains('') && !realNode.classList.contains('')));
    });

    prog.modifyClassSuffix(noJsNode, 'noJs', 'js');
    it('should return TRUE for (noJsNode, "noJs", "js")', function() {
      assert.deepStrictEqual(true,
          (noJsNode.classList.contains('nav--js') && !noJsNode.classList.contains('nav--noJs')));
    });

    prog.modifyClassSuffix(noJsNode2, 'nav--noJs', 'nav--js');
    it('should return TRUE for (noJsNode2, "nav--noJs", "nav--js")', function() {
      assert.deepStrictEqual(true,
          (noJsNode2.classList.contains('nav--js') && !noJsNode2.classList.contains('nav--noJs')));
    });
  }
};
