/** Initialization  ------------------------------ */
/** Node assertion library. */
const assert = require('assert');
/** Functions to test. */
const program = require('./main_test.js');
/** Base general test inputs. */
const inputs = require('./general_specs.js');

/** Function tests. */
const createInteractiveObj =
    require('./test_createInteractiveElementObject/test.js');
const createSxnObjects = require('./test_createSxnObjects/test.js');
const createTextToggleBtnObj =
    require('./test_createTextToggleButtonObj/test.js');
const formServerResponse = require('./test_formServerResponse/test.js');
const getAssociatedElementNode =
    require('./test_getAssociatedElementNode/test.js');
const getCopyright = require('./test_getCopyright/test.js');
const getFormInputError = require('./test_getFormInputError/test.js');
const getFormInputLabel = require('./test_getFormInputLabel/test.js');
const getNavSxns = require('./test_getNavSxns/test.js');
const insertCopyright = require('./test_insertCopyright/test.js');
const isElementNode = require('./test_isElementNode/test.js');
const isEmpty = require('./test_isEmpty/test.js');
const isEmptyInputData = require('./test_isEmptyInputData/test.js');
const isNumber = require('./test_isNumber/test.js');
const isPositiveNumber = require('./test_isPositiveNumber/test.js');
const isSubmitButton = require('./test_isSubmitButton/test.js');
const isValidInputEmail = require('./test_isValidInputEmail/test.js');
const modifyClassSuffix = require('./test_modifyClassSuffix/test.js');
const setAdblockAttribute = require('./test_setAdblockAttribute/test.js');
const setSiteNavLinks = require('./test_setSiteNavLinks/test.js');
const setSiteNavScrollEvent = require('./test_setSiteNavScrollEvent/test.js');
const strEndsWith = require('./test_strEndsWith/test.js');
const toggleDataAttr = require('./test_toggleDataAttribute/test.js');
const toggleNav = require('./test_toggleNav/test.js');
const toggleTextToggleBtn = require('./test_toggleTextToggleButton/test.js');
const validAdblockObj = require('./test_validAdblockObj/test.js');
const validData = require('./test_validData/test.js');
const validFormObj = require('./test_validFormObj/test.js');
const validInteractiveObj =
    require('./test_validInteractiveElementObj/test.js');
const validPageObj = require('./test_validPageObj/test.js');
const validSxnObj = require('./test_validSxnObj/test.js');
const validTextToggleBtnObj = require('./test_validTextToggleBtnObj/test.js');

/** Test Start ------------------------------ */
/** Library Tests. */
describe('Library Functions', function() {
  describe('isElementNode',
      isElementNode.isElementNodeTest.bind(null, assert, program, inputs));
  describe('isNumber',
      isNumber.isNumberTest.bind(null, assert, program, inputs));
  describe('isPositiveNumber', isPositiveNumber.isPositiveNumberTest.bind(
        null, assert, program, inputs));
  describe('isEmpty', isEmpty.isEmptyTest.bind(null, assert, program, inputs));
  describe('toggleDataAttribute', toggleDataAttr.toggleDataAttributeTest.bind(
        null, assert, program, inputs));
  describe('strEndsWith',
      strEndsWith.strEndsWithTest.bind(null, assert, program, inputs));
  describe('modifyClassSuffix', modifyClassSuffix.modifyClassSuffixTest.bind(
        null, assert, program, inputs));
  describe('getCopyright',
      getCopyright.getCopyrightTest.bind(null, assert, program, inputs));
  describe('createInteractiveElementObj',
      createInteractiveObj.createInteractiveElementObjTest.bind(
        null, assert, program, inputs));
  describe('validInteractiveElementObj',
      validInteractiveObj.validInteractiveElementObjTest.bind(
        null, assert, program, inputs));
  describe('createTextToggleButtonObj',
      createTextToggleBtnObj.createTextToggleButtonObjTest.bind(
        null, assert, program, inputs));
  describe('validTextToggleBtnObj',
      validTextToggleBtnObj.validTextToggleBtnObjTest.bind(
        null, assert, program, inputs));
  describe('toggleTextToggleButton',
      toggleTextToggleBtn.toggleTextToggleButtonTest.bind(
        null, assert, program, inputs));
  describe('isEmptyInputData', isEmptyInputData.isEmptyInputDataTest.bind(
      null, assert, program, inputs));
  describe('isValidInputEmail', isValidInputEmail.isValidInputEmailTest.bind(
      null, assert, program, inputs));
  describe('getAssociatedElementNode',
      getAssociatedElementNode.getAssociatedElementNodeTest.bind(
        null, assert, program, inputs));
});

/** Helper Functions: 2nd Level Tests. */
describe('Helper Functions: 2nd Level', function() {
  describe('validAdblockObj',
      validAdblockObj.validAdblockObjTest.bind(null, assert, program, inputs));
  describe('validFormObj',
      validFormObj.validFormObjTest.bind(null, assert, program, inputs));
  describe('validPageObj',
      validPageObj.validPageObjTest.bind(null, assert, program, inputs));
  describe('validSxnObj',
      validSxnObj.validSxnObjTest.bind(null, assert, program, inputs));
  describe('createSxnObjects', createSxnObjects.createSxnObjectsTest.bind(
      null, assert, program, inputs));
  describe('getNavSxns',
      getNavSxns.getNavSxnsTest.bind(null, assert, program, inputs));
  describe('setSiteNavLinks',
      setSiteNavLinks.setSiteNavLinksTest.bind(null, assert, program, inputs));
  describe('setSiteNavScrollEvent',
      setSiteNavScrollEvent.setSiteNavScrollEventTest.bind(
        null, assert, program, inputs));
  describe('getFormInputError', getFormInputError.getFormInputErrorTest.bind(
      null, assert, program, inputs));
  describe('getFormInputLabel', getFormInputLabel.getFormInputLabelTest.bind(
      null, assert, program, inputs));
  describe('formServerResponse', formServerResponse.formServerResponseTest.bind(
      null, assert, program, inputs));
});

/** Helper Functions: 1st Level Tests. */
describe('Helper Functions: 1st Level', function() {
  describe('setAdblockAttribute',
      setAdblockAttribute.setAdblockAttributeTest.bind(
        null, assert, program, inputs));
  describe('insertCopyright',
      insertCopyright.insertCopyrightTest.bind(null, assert, program, inputs));
  describe('toggleNav',
      toggleNav.toggleNavTest.bind(null, assert, program, inputs));
  describe('validData',
      validData.validDataTest.bind(null, assert, program, inputs));
  describe('isSubmitButton',
      isSubmitButton.isSubmitButtonTest.bind(null, assert, program, inputs));
});
