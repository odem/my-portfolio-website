/** strEndsWith =========================================================== */

module.exports = {
  strEndsWithTest: function strEndsWithTest(assert, prog, input) {
    it('Should return FALSE for (null, STR_LONG) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(null, input.STR_LONG));
    });

    it('Should return FALSE for (STR_LONG, null) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_LONG, null));
    });

    it('Should return FALSE for (STR_EMPTY, STR_EMPTY) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_EMPTY, input.STR_EMPTY));
    });

    it('Should return FALSE for (STR_EMPTY, STR_EMPTY, STR_EMPTY) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY));
    });

    it('Should return FALSE for (STR_EMPTY, STR_EMPTY, INT_POS) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_EMPTY, input.STR_EMPTY, input.INT_POS));
    });

    it('Should return FALSE for (STR_LONG, STR_LONG, (STR_LONG.length - 1)) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_LONG, input.STR_LONG, (input.STR_LONG.length - 1)));
    });

    it('Should return FALSE for (STR_SHORT, STR_LONG, STR_SHORT.length) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_SHORT, input.STR_LONG, input.STR_SHORT.length));
    });

    it('Should return FALSE for (STR_SHORT, STR_LONG, (STR_SHORT.length + 1)) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_SHORT, input.STR_LONG, (input.STR_SHORT.length + 1)));
    });

    it('Should return FALSE for (STR_SHORT, STR_LONG, STR_LONG.length) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_SHORT, input.STR_LONG, input.STR_LONG.length));
    });

    it('Should return FALSE for (STR_SHORT, STR_LONG) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_SHORT, input.STR_LONG));
    });

    it('Should return FALSE for (STR_SHORT, STR_EMPTY) input', function() {
      assert.deepStrictEqual(false, prog.strEndsWith(input.STR_SHORT, input.STR_EMPTY));
    });

    it('Should return TRUE for (STR_LONG, STR_LONG) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_LONG));
    });

    it('Should return TRUE for (STR_LONG, STR_SHORT) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_SHORT));
    });

    it('Should return TRUE for (STR_LONG, STR_LONG, null) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_LONG, null));
    });

    it('Should return TRUE for (STR_LONG, STR_LONG, STR_LONG.length) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_LONG, input.STR_LONG.length));
    });

    it('Should return TRUE for (STR_LONG, STR_LONG, (STR_LONG.length + 1)) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_LONG, (input.STR_LONG.length + 1)));
    });

    it('Should return TRUE for (STR_LONG, STR_SHORT, input.STR_LONG.length) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_SHORT, input.STR_LONG.length));
    });

    it('Should return TRUE for (STR_LONG, STR_SHORT, input.STR_SHORT.length) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_SHORT, input.STR_SHORT.length));
    });

    it('Should return TRUE for (STR_LONG, STR_SHORT, (input.STR_SHORT.length + 1)) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_SHORT, (input.STR_SHORT.length + 1)));
    });

    it('Should return TRUE for (STR_LONG, STR_LONG, INT_NEG) input', function() {
      assert.deepStrictEqual(true, prog.strEndsWith(input.STR_LONG, input.STR_LONG, input.INT_NEG));
    });
  }
};
