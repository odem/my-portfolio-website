/** validFormObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar">' +
      '<div id="child"></div>' +
    '</div>' +
    '<div id="sibling"></div>' +
    '<form class="js-SxnContact-form" method="post" action="mailer.php">' +
      '<div class="js-SxnContact-formMsg" data-submit="none">' +
        '<p class="js-SxnContact-formMsgText" data-msg="none"></p>' +
      '</div>' +
      '<div class="Form-element">' +
        '<label for="name" class="js-Form-label" data-error="false">Name' +
          '<span class="Form-required">*</span>' +
        '</label>' +
        '<input class="js-Form-input" id="name" name="name" data-error="false" type="text" required>' +
        '<div class="js-Form-inputErrorMsg" data-error="false">A name must be entered.</div>' +
      '</div>' +
    '</form>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const childNode = document.getElementById('child');
const siblingNode = document.getElementById('sibling');
const formNodes = document.getElementsByClassName('js-SxnContact-form');
const inputNodes = document.getElementsByClassName('js-Form-input');
const formMsgNode = document.getElementsByClassName('js-SxnContact-formMsg')[0];
const formMsgTextNode = document.getElementsByClassName('js-SxnContact-formMsgText')[0];
const sucMsg = 'success message.';
const failMsg = 'failure message.'
/** FormComponents Objects */
const formObj = {
  form: realNode,
  inputs: [1,2,2,5],
  formMsg: childNode,
  formMsgText: siblingNode,
  successMsg: 'success',
  failMsg: 'fail',
};
const formObjBad = {
  form: realNode,
  inputs: [1,2,2,5],
  formMsg: 'childNode',
  formMsgText: siblingNode,
  successMsg: 'success',
  failMsg: 'fail',
};
const formObjReal = {
  form: formNodes[0],
  inputs: inputNodes,
  formMsg: formMsgNode,
  formMsgText: formMsgTextNode,
  successMsg: sucMsg,
  failMsg: failMsg,
};

/** Test  ------------------------------ */
module.exports = {
  validFormObjTest: function validFormObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(realNode));
    });

    it('should return FALSE for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({element:input.OBJ_EMPTY}));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({element:realNode}));
    });

    it('should return FALSE for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual(false,
          prog.validFormObj({element:input.OBJ_EMPTY, attribute:'data-empty'}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.validFormObj({element:input.ELEMENT_NODE_FAKE, name:'fake'}));
    });

    it('should return FALSE for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.validFormObj({element:realNode, name:'foobar'}));
    });

    it('should return FALSE for {name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({name:'foobar'}));
    });

    it('should return FALSE for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({attribute:'foobar'}));
    });

    it('should return FALSE for {container:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validFormObj({container:realNode}));
    });

    it('should return FALSE for {element:realNode, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual(false,
          prog.validFormObj({element:realNode, attribute:'foobarNode'}));
    });
    it('should return FALSE for formObjBad input', function() {
      assert.deepStrictEqual(false, prog.validFormObj(formObjBad));
    });

    it('should return TRUE for formObj input', function() {
      assert.deepStrictEqual(true, prog.validFormObj(formObj));
    });

    it('should return TRUE for formObjReal input', function() {
      assert.deepStrictEqual(true, prog.validFormObj(formObjReal));
    });
  }
};
