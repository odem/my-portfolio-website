/** setAdblockAttribute =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar">' +
      '<div id="child"></div>' +
    '</div>' +
    '<div id="baz" data-baz="none">' +
      '<div id="bazChild"></div>' +
    '</div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const foobar = document.getElementById('foobar');
const foobarChild = document.getElementById('child');
const baz = document.getElementById('baz');
const bazChild = document.getElementById('bazChild');
/** getPropertyValue('display') of adblockObj.element = block */
/** AdblockComponents Objects */
const adblockObj = {
  element: foobarChild,
  container: foobar,
};
const adblockObjBad = {
  element: 'foobarNode',
  container: foobarChild,
};
const adblockObjAttr = {
  element: bazChild,
  container: baz,
  attribute: 'data-baz',
};
const adblockObjAttr2 = {
  element: bazChild,
  container: baz,
  attribute: 'data-foo',
};
const adblockObjAttrBad = {
  element: bazChild,
  container: baz,
  attribute: bazChild,
};

/** Test  ------------------------------ */
module.exports = {
  setAdblockAttributeTest: function setAdblockAttributeTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(true, window));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(false, window));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(null, window));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(undefined, window));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(NaN, window));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.INT_POS, window));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.INT_NEG, window));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.INT_ZERO_POS, window));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.INT_ZERO_NEG, window));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.FLOAT_POS, window));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.FLOAT_NEG, window));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.FLOAT_ZERO_POS, window));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.FLOAT_ZERO_NEG, window));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.STR_EMPTY, window));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.STR_LONG, window));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_EMPTY, window));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_NUM, window));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_STR, window));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_ARRAY_NUM, window));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_ARRAY_STR, window));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_OBJ_EMPTY, window));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ARRAY_OBJ, window));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.OBJ, window));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.OBJ_EMPTY, window));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(input.ELEMENT_NODE_FAKE, window));
    });

    it('Should return FALSE for foobar input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(foobar, window));
    });

    it('should return FALSE for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({element:input.OBJ_EMPTY}, window));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({element:input.ELEMENT_NODE_FAKE}, window));
    });

    it('should return FALSE for {element:foobar} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({element:foobar}, window));
    });

    it('should return FALSE for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual(false,
          prog.setAdblockAttribute({element:input.OBJ_EMPTY, attribute:'data-empty'}, window));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.setAdblockAttribute({element:input.ELEMENT_NODE_FAKE, name:'fake'}, window));
    });

    it('should return FALSE for {element:foobar, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.setAdblockAttribute({element:foobar, name:'foobar'}, window));
    });

    it('should return FALSE for {name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({name:'foobar'}, window));
    });

    it('should return FALSE for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({attribute:'foobar'}, window));
    });

    it('should return FALSE for {container:foobar} input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute({container:foobar}, window));
    });

    it('should return FALSE for {element:foobar, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual(false,
          prog.setAdblockAttribute({element:foobar, attribute:'foobarNode'}, window));
    });

    it('should return FALSE for adblockObjBad input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(adblockObjBad, window));
    });

    it('should return FALSE for adblockObjAttrBad input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(adblockObjBad, window));
    });

    it('should return FALSE for adblockObjAttrBad input', function() {
      assert.deepStrictEqual(false, prog.setAdblockAttribute(adblockObjAttrBad, window));
    });

    prog.setAdblockAttribute(adblockObjAttr, window);
    it('should return TRUE for adblockObjAttr input', function() {
      assert.deepStrictEqual(true, adblockObjAttr.container.getAttribute('data-baz') === 'false');
    });

    prog.setAdblockAttribute(adblockObjAttr2, window);
    it('should return TRUE for adblockObjAttr2 input', function() {
      assert.deepStrictEqual(true, adblockObjAttr2.container.getAttribute('data-foo') === 'false');
    });

    prog.setAdblockAttribute(adblockObj, window);
    it('should return TRUE for adblockObj input', function() {
      assert.deepStrictEqual(true, adblockObj.container.getAttribute('data-blocked') === 'false');
    });
  }
};
