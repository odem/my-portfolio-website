/** validPageObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div class="multiple" id="sxn1Link"></div>' +
    '<div class="multiple" id="sxn2Link"></div>' +
    '<div class="multiple" id="sxn3Link"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const multipleNodes = document.getElementsByClassName('multiple');
/** PageComponents Object */
const pageObj = {
  sxns: multipleNodes,
  navSxns: [1,2,3,4],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};
const pageObjBad = {
  sxns: multipleNodes,
  navSxns: [],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};

/** Test  ------------------------------ */
module.exports = {
  validPageObjTest: function validPageObjTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(realNode));
    });

    it('should return FALSE for {element:OBJ_EMPTY} input', function() {
      assert.deepStrictEqual(false, prog.validPageObj({element:input.OBJ_EMPTY}));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.validPageObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return FALSE for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.validPageObj({element:realNode}));
    });

    it('should return FALSE for {element:OBJ_EMPTY, attribute:"data-empty"}', function() {
      assert.deepStrictEqual(false,
          prog.validPageObj({element:input.OBJ_EMPTY, attribute:'data-empty'}));
    });

    it('should return FALSE for {element:input.ELEMENT_NODE_FAKE, name:"fake"}', function() {
      assert.deepStrictEqual(false,
          prog.validPageObj({element:input.ELEMENT_NODE_FAKE, name:'fake'}));
    });

    it('should return FALSE for {element:realNode, name:"foobar"}', function() {
      assert.deepStrictEqual(false,
          prog.validPageObj({element:realNode, name:'foobar'}));
    });

    it('should return FALSE for {name:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validPageObj({name:'foobar'}));
    });

    it('should return FALSE for {attribute:"foobar"} input', function() {
      assert.deepStrictEqual(false, prog.validPageObj({attribute:'foobar'}));
    });

    it('should return FALSE for {element:realNode, attribute:"foobarNode"} input', function() {
      assert.deepStrictEqual(false,
          prog.validPageObj({element:realNode, attribute:'foobarNode'}));
    });

    it('should return FALSE for pageObjBad input', function() {
      assert.deepStrictEqual(false, prog.validPageObj(pageObjBad));
    });

    it('should return TRUE for pageObj input', function() {
      assert.deepStrictEqual(true, prog.validPageObj(pageObj));
    });
  }
};
