/** createTextToggleButtonObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id=foobar></div>' +
    '<div id=ariaPressedFalse aria-pressed=false></div>' +
    '<div id=ariaPressedTrue aria-pressed=true></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const ariaPressedFalseNode = document.getElementById('ariaPressedFalse');
const ariaPressedTrueNode = document.getElementById('ariaPressedTrue');
/** TextToggleButton Objects */
const textToggleButton = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: '&times;',
  notPressedText: 'Menu',
};
const textToggleButtonName = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: '&times;',
  notPressedText: 'Menu',
};
const textToggleButtonText = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: 'foo',
  notPressedText: 'bar',
};
const textToggleButtonNameText = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  notPressedText: 'bar',
  pressedText: 'foo',
};
const textToggleButtonFalse = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: ariaPressedFalseNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const textToggleButtonTrue = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: ariaPressedTrueNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};

/** Test  ------------------------------ */
module.exports = {
  createTextToggleButtonObjTest: function createTextToggleButtonObjTest(assert, prog, input) {
    it('Should return {} for true input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(true));
    });

    it('Should return {} for false{} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(false));
    });

    it('Should return {} for null input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(null));
    });

    it('Should return {} for undefined input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(undefined));
    });

    it('Should return {} for NaN input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(NaN));
    });

    it('Should return {} for INT_POS input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.INT_POS));
    });

    it('Should return {} for INT_NEG input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.INT_NEG));
    });

    it('Should return {} for INT_ZERO_POS input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.INT_ZERO_POS));
    });

    it('Should return {} for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.INT_ZERO_NEG));
    });

    it('Should return {} for FLOAT_POS input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.FLOAT_POS));
    });

    it('Should return {} for FLOAT_NEG input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.FLOAT_NEG));
    });

    it('Should return {} for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.FLOAT_ZERO_POS));
    });

    it('Should return {} for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return {} for STR_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.STR_EMPTY));
    });

    it('Should return {} for STR_LONG input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.STR_LONG));
    });

    it('Should return {} for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_EMPTY));
    });

    it('Should return {} for ARRAY_NUM input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_NUM));
    });

    it('Should return {} for ARRAY_STR input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_STR));
    });

    it('Should return {} for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return {} for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return {} for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return {} for ARRAY_OBJ input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ARRAY_OBJ));
    });

    it('Should return {} for OBJ input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.OBJ));
    });

    it('Should return {} for OBJ_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.OBJ_EMPTY));
    });

    it('Should return {} for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj(input.ELEMENT_NODE_FAKE));
    });

    it('should return TextToggleButton for realNode input', function() {
      assert.deepStrictEqual(textToggleButton, prog.createTextToggleButtonObj(realNode));
    });

    it('should return {} for {element:input.OBJ} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj({element:input.OBJ}));
    });

    it('should return {} for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return TextToggleButton for {element:realNode} input', function() {
      assert.deepStrictEqual(textToggleButton, prog.createTextToggleButtonObj({element:realNode}));
    });

    it('should return {} for {element:OBJ, attribute:data-empty} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj({element:input.OBJ, attribute:'data-empty'}));
    });

    it('should return {} for {element:input.OBJ, name:fake} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj({element:input.OBJ, name:'fake'}));
    });

    it('should return textToggleButtonName for {element:realNode, name:foobar}', function() {
      assert.deepStrictEqual(textToggleButtonName,
          prog.createTextToggleButtonObj({element:realNode, name:'foobar'}));
    });

    it('should return textToggleButtonText for ' +
        '{element:realNode, pressedText:foo, notPressedText: bar}', function() {
      assert.deepStrictEqual(textToggleButtonText,
          prog.createTextToggleButtonObj({element:realNode, pressedText:'foo', notPressedText: 'bar'}));
    });

    it('should return {} for {name: foobar} input', function() {
      assert.deepStrictEqual({}, prog.createTextToggleButtonObj({name:'foobar'}));
    });

    it('should return textToggleButton for {element:realNode, other: random, prop: prop}', function() {
      assert.deepStrictEqual(textToggleButton,
          prog.createTextToggleButtonObj({element:realNode, other: 'random', prop: 'prop'}));
    });

    it('should return textToggleButtonText for ' +
        ' {element:realNode, pressedText:foo, notPressedText: bar, other: random, prop: prop}', function() {
      assert.deepStrictEqual(textToggleButtonText,
          prog.createTextToggleButtonObj(
            {element:realNode, pressedText:'foo', notPressedText: 'bar', other: 'random', prop: 'prop'}));
    });

    it('should return textToggleButtonNameText for ' +
        ' {element:realNode, pressedText:foo, notPressedText: bar, name: foobar}', function() {
      assert.deepStrictEqual(textToggleButtonNameText,
          prog.createTextToggleButtonObj(
            {element:realNode, pressedText:'foo', notPressedText: 'bar', name: 'foobar'}));
    });
  }
};
