/** getAssociatedElementNode =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar">' +
      '<div class="foobarChild"></div>' +
    '</div>' +
    '<div class="foobarSibling"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const childNode = document.getElementsByClassName('foobarChild')[0];
const siblingNode = document.getElementsByClassName('foobarSibling')[0];

/** Test  ------------------------------ */
module.exports = {
  getAssociatedElementNodeTest: function getAssociatedElementNodeTest(assert, prog, input) {
    it('should return NULL for (realNode, true) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, true));
    });

    it('should return NULL for (true, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(true, 'child'));
    });

    it('should return NULL for (true, true) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(true, true));
    });

    it('should return NULL for (realNode, false) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, false));
    });

    it('should return NULL for (false, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(false, 'child'));
    });

    it('should return NULL for (false, false) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(false, false));
    });

    it('should return NULL for (realNode, undefined) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, undefined));
    });

    it('should return NULL for (undefined, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(undefined, 'child'));
    });

    it('should return NULL for (undefined, undefined) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(undefined, undefined));
    });

    it('should return NULL for (realNode, undefined) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, undefined));
    });

    it('should return NULL for (undefined, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(undefined, 'child'));
    });

    it('should return NULL for (undefined, undefined) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(undefined, undefined));
    });

    it('should return NULL for (realNode, null) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, null));
    });

    it('should return NULL for (null, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(null, 'child'));
    });

    it('should return NULL for (null, null) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(null, null));
    });

    it('should return NULL for (realNode, NaN) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, NaN));
    });

    it('should return NULL for (NaN, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(NaN, 'child'));
    });

    it('should return NULL for (NaN, NaN) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(NaN, NaN));
    });

    it('should return NULL for (realNode, input.INT_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.INT_POS));
    });

    it('should return NULL for (input.INT_POS, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_POS, 'child'));
    });

    it('should return NULL for (input.INT_POS, input.INT_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_POS, input.INT_POS));
    });

    it('should return NULL for (realNode, input.INT_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.INT_NEG));
    });

    it('should return NULL for (input.INT_NEG, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_NEG, 'child'));
    });

    it('should return NULL for (input.INT_NEG, input.INT_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_NEG, input.INT_NEG));
    });

    it('should return NULL for (realNode, input.INT_ZERO_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.INT_ZERO_POS));
    });

    it('should return NULL for (input.INT_ZERO_POS, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_ZERO_POS, 'child'));
    });

    it('should return NULL for (input.INT_ZERO_POS, input.INT_ZERO_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_ZERO_POS, input.INT_ZERO_POS));
    });

    it('should return NULL for (realNode, input.INT_ZERO_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.INT_ZERO_NEG));
    });

    it('should return NULL for (input.INT_ZERO_NEG, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_ZERO_NEG, 'child'));
    });

    it('should return NULL for (input.INT_ZERO_NEG, input.INT_ZERO_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.INT_ZERO_NEG, input.INT_ZERO_NEG));
    });

    it('should return NULL for (realNode, input.FLOAT_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.FLOAT_POS));
    });

    it('should return NULL for (input.FLOAT_POS, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_POS, 'child'));
    });

    it('should return NULL for (input.FLOAT_POS, input.FLOAT_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_POS, input.FLOAT_POS));
    });

    it('should return NULL for (realNode, input.FLOAT_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.FLOAT_NEG));
    });

    it('should return NULL for (input.FLOAT_NEG, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_NEG, 'child'));
    });

    it('should return NULL for (input.FLOAT_NEG, input.FLOAT_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_NEG, input.FLOAT_NEG));
    });

    it('should return NULL for (realNode, input.FLOAT_ZERO_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.FLOAT_ZERO_POS));
    });

    it('should return NULL for (input.FLOAT_ZERO_POS, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_ZERO_POS, 'child'));
    });

    it('should return NULL for (input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS));
    });

    it('should return NULL for (realNode, input.FLOAT_ZERO_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.FLOAT_ZERO_NEG));
    });

    it('should return NULL for (input.FLOAT_ZERO_NEG, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_ZERO_NEG, 'child'));
    });

    it('should return NULL for (input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG));
    });

    it('should return NULL for (realNode, input.STR_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.STR_EMPTY));
    });

    it('should return NULL for (input.STR_EMPTY, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.STR_EMPTY, 'child'));
    });

    it('should return NULL for (input.STR_EMPTY, input.STR_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.STR_EMPTY, input.STR_EMPTY));
    });

    it('should return NULL for (realNode, input.STR_LONG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.STR_LONG));
    });

    it('should return NULL for (input.STR_LONG, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.STR_LONG, 'child'));
    });

    it('should return NULL for (input.STR_LONG, input.STR_LONG) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.STR_LONG, input.STR_LONG));
    });

    it('should return NULL for (realNode, input.ARRAY_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_EMPTY));
    });

    it('should return NULL for (input.ARRAY_EMPTY, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_EMPTY, 'child'));
    });

    it('should return NULL for (input.ARRAY_EMPTY, input.ARRAY_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_EMPTY, input.ARRAY_EMPTY));
    });

    it('should return NULL for (realNode, input.ARRAY_NUM) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_NUM));
    });

    it('should return NULL for (input.ARRAY_NUM, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_NUM, 'child'));
    });

    it('should return NULL for (input.ARRAY_NUM, input.ARRAY_NUM) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_NUM, input.ARRAY_NUM));
    });

    it('should return NULL for (realNode, input.ARRAY_STR) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_STR));
    });

    it('should return NULL for (input.ARRAY_STR, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_STR, 'child'));
    });

    it('should return NULL for (input.ARRAY_STR, input.ARRAY_STR) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_STR, input.ARRAY_STR));
    });

    it('should return NULL for (realNode, input.ARRAY_ARRAY_NUM) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_ARRAY_NUM));
    });

    it('should return NULL for (input.ARRAY_ARRAY_NUM, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_ARRAY_NUM, 'child'));
    });

    it('should return NULL for (input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM));
    });

    it('should return NULL for (realNode, input.ARRAY_ARRAY_STR) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_ARRAY_STR));
    });

    it('should return NULL for (input.ARRAY_ARRAY_STR, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_ARRAY_STR, 'child'));
    });

    it('should return NULL for (ARRAY_ARRAY_STR, ARRAY_ARRAY_STR) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR));
    });

    it('should return NULL for (realNode, input.ARRAY_OBJ_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_OBJ_EMPTY));
    });

    it('should return NULL for (input.ARRAY_OBJ_EMPTY, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_OBJ_EMPTY, 'child'));
    });

    it('should return NULL for (input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY));
    });

    it('should return NULL for (realNode, input.ARRAY_OBJ) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, input.ARRAY_OBJ));
    });

    it('should return NULL for (input.ARRAY_OBJ, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_OBJ, 'child'));
    });

    it('should return NULL for (input.ARRAY_OBJ, input.ARRAY_OBJ) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(input.ARRAY_OBJ, input.ARRAY_OBJ));
    });

    it('should return NULL for (realNode, "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, 'child'));
    });

    it('should return NULL for ("child", realNode) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode('child', realNode));
    });

    it('should return NULL for ("realNode", "child") input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode('realNode', 'child'));
    });

    it('should return NULL for (realNode, realNode) input', function() {
      assert.deepStrictEqual(null, prog.getAssociatedElementNode(realNode, realNode));
    });

    it('should return childNode for ("foobarChild", realNode) input', function() {
      assert.deepStrictEqual(childNode, prog.getAssociatedElementNode('foobarChild', realNode));
    });

    it('should return siblingNode for ("foobarSibling", realNode) input', function() {
      assert.deepStrictEqual(siblingNode, prog.getAssociatedElementNode('foobarSibling', realNode));
    });
  }
};
