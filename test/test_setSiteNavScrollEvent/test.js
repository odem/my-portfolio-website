/** setSiteNavScrollEvent =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="sibling"></div>' +
    '<div class="multiple" id="sxn1Link"></div>' +
    '<div class="multiple" id="sxn2Link"></div>' +
    '<div class="multiple" id="sxn3Link"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const siblingNode = document.getElementById('sibling');
const multipleNodes = document.getElementsByClassName('multiple');
/** PageComponents Objects */
const pageObj = {
  sxns: multipleNodes,
  navSxns: [1,2,3,4],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};
const pageObjBad = {
  sxns: multipleNodes,
  navSxns: [],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};
/** Scroll events. */
addEvent = "window.addEventListener('scroll', program.setSiteNavLinks.bind(" +
    "null, pageItems.navSxns, pageItems.navLinkAttribute, pageItems.navPercent)," +
    " false)";
removeEvent = "window.removeEventListener('scroll', program.setSiteNavLinks.bind(" +
    "null, pageItems.navSxns, pageItems.navLinkAttribute, pageItems.navPercent)," +
    " false)";

/** Test  ------------------------------ */
module.exports = {
  setSiteNavScrollEventTest: function setSiteNavScrollEventTest(assert, prog, input) {
    it('Should return NULL for BOOL_TRUE input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(true, true, window));
    });

    it('Should return NULL for BOOL_NULL input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(false, false, window));
    });

    it('Should return NULL for NULL input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(null, null, window));
    });

    it('Should return NULL for UNDEEFINED input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(undefined, undefined, window));
    });

    it('Should return NULL for NAN input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(NaN, NaN, window));
    });

    it('Should return NULL for INT_POS input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.INT_POS, input.INT_POS, window));
    });

    it('Should return NULL for INT_NEG input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.INT_NEG, input.INT_NEG, window));
    });

    it('Should return NULL for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.INT_ZERO_POS, input.INT_ZERO_POS, window));
    });

    it('Should return NULL for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.INT_ZERO_NEG, input.INT_ZERO_NEG, window));
    });

    it('Should return NULL for FLOAT_POS input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.FLOAT_POS, input.FLOAT_POS, window));
    });

    it('Should return NULL for FLOAT_NEG input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.FLOAT_NEG, input.FLOAT_NEG, window));
    });

    it('Should return NULL for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, window));
    });

    it('Should return NULL for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, window));
    });

    it('Should return NULL for STR_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.STR_EMPTY, input.STR_EMPTY, window));
    });

    it('Should return NULL for STR_LONG input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.STR_LONG, input.STR_LONG, window));
    });

    it('Should return NULL for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_EMPTY, input.ARRAY_EMPTY, window));
    });

    it('Should return NULL for ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_NUM, input.ARRAY_NUM, window));
    });

    it('Should return NULL for ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_STR, input.ARRAY_STR, window));
    });

    it('Should return NULL for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, window));
    });

    it('Should return NULL for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, window));
    });

    it('Should return NULL for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, window));
    });

    it('Should return NULL for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ARRAY_OBJ, input.ARRAY_OBJ, window));
    });

    it('Should return NULL for OBJ input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.OBJ, input.OBJ, window));
    });

    it('Should return NULL for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.OBJ_EMPTY, input.OBJ_EMPTY, window));
    });

    it('Should return NULL for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, window));
    });

    it('Should return NULL for realNode input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(realNode, realNode, window));
    });

    it('Should return NULL for (realNode, "child") input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(realNode, 'child', window));
    });

    it('Should return NULL for ("child", siblingNode) input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent('child', siblingNode, window));
    });

    it('Should return NULL for (pageObj, "") input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(pageObj, '', window));
    });

    it('Should return NULL for ({}, "add") input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent({}, 'add', window));
    });

    it('Should return NULL for (null, "add") input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(null, 'add', window));
    });

    it('Should return NULL for (pageObjBad, "add") input', function() {
      assert.deepStrictEqual(null, prog.setSiteNavScrollEvent(pageObjBad, 'add', window));
    });

    it('Should return addEvent for (pageObj, "add") input', function() {
      assert.deepStrictEqual(addEvent, prog.setSiteNavScrollEvent(pageObj, 'add', window));
    });

    it('Should return removeEvent for (pageObj, "remove") input', function() {
      assert.deepStrictEqual(removeEvent, prog.setSiteNavScrollEvent(pageObj, 'remove', window));
    });

    it('Should return removeEvent for (pageObj, "pause") input', function() {
      assert.deepStrictEqual(removeEvent, prog.setSiteNavScrollEvent(pageObj, 'pause', window));
    });
  }
};
