/** loadCharts =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div class="chart"></div>' +
    '<div class="chart"></div>' +
    '<div class="chart"></div>' +
    '<div class="sxn"></div>' +
    '<div class="sxn"></div>' +
    '<div class="sxn"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const chartNodes = document.getElementsByClassName('chart');
const sxnNodes = document.getElementsByClassName('sxn');
/** Dummy objects. */
const pageObj = {
  sxns: sxnNodes,
  navSxns: [1,2,3,4],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};
const pageObjBad = {
  sxns: sxnNodes,
  navSxns: [],
  navPercent: .3,
  navLinkAttribute: 'attrName',
  chartsLoaded: false,
};


/** Test  ------------------------------ */
module.exports = {
  loadChartsTest: function loadChartsTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(true, true, true, true, window, document));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(false, false, false, false, window, document));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(null, null, null, null, window, document));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(undefined, undefined, undefined, undefined, window, document));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(NaN, NaN, NaN, NaN, window, document));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.INT_POS, input.INT_POS, input.INT_POS, input.INT_POS, window, document));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.INT_NEG, input.INT_NEG, input.INT_NEG, input.INT_NEG, window, document));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS, input.INT_ZERO_POS, window, document));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG, input.INT_ZERO_NEG, window, document));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS, input.FLOAT_POS, window, document));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG, input.FLOAT_NEG, window, document));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, input.FLOAT_ZERO_POS, window, document));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, input.FLOAT_ZERO_NEG, window, document));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY, input.STR_EMPTY, window, document));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.STR_LONG, input.STR_LONG, input.STR_LONG, input.STR_LONG, window, document));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY, input.ARRAY_EMPTY, window, document));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM, input.ARRAY_NUM, window, document));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR, input.ARRAY_STR, window, document));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, input.ARRAY_ARRAY_NUM, window, document));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, input.ARRAY_ARRAY_STR, window, document));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, input.ARRAY_OBJ_EMPTY, window, document));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ, input.ARRAY_OBJ, window, document));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.OBJ, input.OBJ, input.OBJ, input.OBJ, window, document));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY, input.OBJ_EMPTY, window, document));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, input.ELEMENT_NODE_FAKE, window, document));
    });

    it('Should return FALSE for realNode input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(realNode, realNode, realNode, realNode, window, document));
    });

    it('Should return FALSE for (null, pageObj, 42, 42) input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(null, pageObj, 42, 42, window, document));
    });

    it('Should return FALSE for (chartNodes, null, 42, 42) input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(chartNodes, null, 42, 42, window, document));
    });

    it('Should return FALSE for (chartNodes, pageObj, -42, 42) input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(chartNodes, pageObj, -42, 42, window, document));
    });

    it('Should return FALSE for (chartNodes, pageObj, 42, -42) input', function() {
      assert.deepStrictEqual(false, prog.loadCharts(chartNodes, pageObj, 42, -42, window, document));
    });

    it('Should return TRUE for (chartNodes, pageObj, 0, 42) input', function() {
      assert.deepStrictEqual(true, prog.loadCharts(chartNodes, pageObj, 0, 42, window, document));
    });

    it('Should return TRUE for (chartNodes, pageObj, -0, 42) input', function() {
      assert.deepStrictEqual(true, prog.loadCharts(chartNodes, pageObj, -0, 42, window, document));
    });

    it('Should return TRUE for (chartNodes, pageObj, 42, 0) input', function() {
      assert.deepStrictEqual(true, prog.loadCharts(chartNodes, pageObj, 42, 0, window, document));
    });

    it('Should return TRUE for (chartNodes, pageObj, 42, -0) input', function() {
      assert.deepStrictEqual(true, prog.loadCharts(chartNodes, pageObj, 42, -0, window, document));
    });

    it('Should return TRUE for (chartNodes, pageObj, 42, 42) input', function() {
      assert.deepStrictEqual(true, prog.loadCharts(chartNodes, pageObj, 42, 42, window, document));
    });
  }
};
