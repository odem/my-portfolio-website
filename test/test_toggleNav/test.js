/** toggleNav =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="element" aria-pressed="true"></div>' +
    '<div id="button" aria-pressed="true"></div>' +
    '<div id="element2" aria-pressed="true"></div>' +
    '<div id="button2" aria-pressed="true"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const elementNode = document.getElementById('element');
const buttonNode = document.getElementById('button');
const elementNode2 = document.getElementById('element2');
const buttonNode2 = document.getElementById('button2');
/** Dummy objects. */
const interactiveElement = {
  name: 'interactiveElement',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: elementNode,
};
const textToggleButton = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: buttonNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const interactiveElementBad = {
  name: 'interactiveElement',
  attribute: 'aria-disabled',
  attributeState1: 'true',
  attributeState2: 'false',
  element: elementNode,
};
const textToggleButtonBad = {
  name: 'textToggleBtn',
  attribute: 'aria-disabled',
  attributeState1: 'true',
  attributeState2: 'false',
  element: buttonNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const interactiveElement2 = {
  name: 'interactiveElement',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: elementNode2,
};
const textToggleButton2 = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: buttonNode2,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const interactiveElementBad2 = {
  name: 'interactiveElement',
  attribute: 'aria-disabled',
  attributeState1: 'true',
  attributeState2: 'false',
  element: elementNode2,
};
const textToggleButtonBad2 = {
  name: 'textToggleBtn',
  attribute: 'aria-disabled',
  attributeState1: 'true',
  attributeState2: 'false',
  element: buttonNode2,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const arrayGood = [interactiveElement, textToggleButton];
const arrayBad = [interactiveElementBad, textToggleButtonBad];
const objGood = {
  interactiveElement: interactiveElement2,
  textToggleButton: textToggleButton2,
};
const objBad = {
  interactiveElement: interactiveElementBad2,
  textToggleButton: textToggleButtonBad2,
};
var arrayGoodValid = false;
var arrayBadValid = false;
var objGoodValid = false;
var objBadValid = false;


/** Test  ------------------------------ */
module.exports = {
  toggleNavTest: function toggleNavTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.toggleNav(input.STR_LONG));
    });

    it('Should return UNDEFINED for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_EMPTY));
    });

    it('Should return UNDEFINED for ARRAY_NUM input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_NUM));
    });

    it('Should return UNDEFINED for ARRAY_STR input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_STR));
    });

    it('Should return UNDEFINED for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_ARRAY_NUM));
    });

    it('Should return UNDEFINED for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_ARRAY_STR));
    });

    it('Should return UNDEFINED for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return UNDEFINED for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.ARRAY_OBJ));
    });

    it('Should return UNDEEFINED for OBJ input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.OBJ));
    });

    it('Should return UNDEEFINED for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(input.OBJ_EMPTY));
    });

    it('Should return UNDEEFINED for realNode input', function() {
      assert.deepStrictEqual(undefined, prog.toggleNav(realNode));
    });

    prog.toggleNav(arrayBad);
    arrayBadValid =
        (interactiveElementBad.element.getAttribute(interactiveElementBad.attribute) === interactiveElementBad.attributeState2) &&
        (textToggleButtonBad.element.getAttribute(textToggleButtonBad.attribute) === textToggleButtonBad.attributeState2) &&
        (textToggleButtonBad.element.innerHTML === textToggleButtonBad.notPressedText);
    it('Should return FALSE for arrayBad input', function() {
      assert.deepStrictEqual(false, arrayBadValid);
    });

    prog.toggleNav(objBad);
    objBadValid =
        (interactiveElementBad2.element.getAttribute(interactiveElementBad2.attribute) === interactiveElementBad2.attributeState2) &&
        (textToggleButtonBad2.element.getAttribute(textToggleButtonBad2.attribute) === textToggleButtonBad2.attributeState2) &&
        (textToggleButtonBad2.element.innerHTML === textToggleButtonBad2.notPressedText);
    it('Should return FALSE for arrayBad input', function() {
      assert.deepStrictEqual(false, objBadValid);
    });

    prog.toggleNav(arrayGood);
    arrayGoodValid =
        (interactiveElement.element.getAttribute(interactiveElement.attribute) === interactiveElement.attributeState2) &&
        (textToggleButton.element.getAttribute(textToggleButton.attribute) === textToggleButton.attributeState2) &&
        (textToggleButton.element.innerHTML === textToggleButton.notPressedText);
    it('Should return TRUE for arrayGood input', function() {
      assert.deepStrictEqual(true, arrayGoodValid);
    });

    prog.toggleNav(objGood);
    objGoodValid =
        (interactiveElement2.element.getAttribute(interactiveElement2.attribute) === interactiveElement2.attributeState2) &&
        (textToggleButton2.element.getAttribute(textToggleButton2.attribute) === textToggleButton2.attributeState2) &&
        (textToggleButton2.element.innerHTML === textToggleButton2.notPressedText);
    it('Should return TRUE for objGood input', function() {
      assert.deepStrictEqual(true, objGoodValid);
    });
  }
};
