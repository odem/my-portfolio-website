/** toggleTextToggleButton =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
    '<div id="true" aria-pressed="true"></div>' +
    '<div id="false" aria-pressed="false"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
const trueNode = document.getElementById('true');
const falseNode = document.getElementById('false');
/** TextToggleButton Objects */
const textToggleButtonTrue = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: trueNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const textToggleButtonFalse = {
  name: 'textToggleBtn',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: falseNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
const textToggleButtonBad = {
  name: 'foobar',
  attribute: 'aria-disabled',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
  pressedText: 'x',
  notPressedText: 'Menu',
};
propertiesSetCorrectly = false;

/** Test  ------------------------------ */
module.exports = {
  toggleTextToggleButtonTest: function toggleTextToggleButtonTest(assert, prog, input) {
    it('Should return FALSE for true input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(true));
    });

    it('Should return FALSE for false input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(false));
    });

    it('Should return FALSE for null input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(null));
    });

    it('Should return FALSE for undefined input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(undefined));
    });

    it('Should return FALSE for NaN input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(NaN));
    });

    it('Should return FALSE for INT_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.INT_POS));
    });

    it('Should return FALSE for INT_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.INT_NEG));
    });

    it('Should return FALSE for INT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.INT_ZERO_POS));
    });

    it('Should return FALSE for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.INT_ZERO_NEG));
    });

    it('Should return FALSE for FLOAT_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.FLOAT_POS));
    });

    it('Should return FALSE for FLOAT_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.FLOAT_NEG));
    });

    it('Should return FALSE for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.FLOAT_ZERO_POS));
    });

    it('Should return FALSE for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.FLOAT_ZERO_NEG));
    });

    it('Should return FALSE for STR_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.STR_EMPTY));
    });

    it('Should return FALSE for STR_LONG input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.STR_LONG));
    });

    it('Should return FALSE for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_EMPTY));
    });

    it('Should return FALSE for ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_ARRAY_NUM));
    });

    it('Should return FALSE for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_ARRAY_STR));
    });

    it('Should return FALSE for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return FALSE for ARRAY_OBJ input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ARRAY_OBJ));
    });

    it('Should return FALSE for OBJ input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.OBJ));
    });

    it('Should return FALSE for OBJ_EMPTY input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.OBJ_EMPTY));
    });

    it('Should return FALSE for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(input.ELEMENT_NODE_FAKE));
    });

    it('should return false for realNode input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(realNode));
    });

    it('should return FALSE for {element:input.OBJ} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({element:input.OBJ}));
    });

    it('should return FALSE for {element:ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({element:input.ELEMENT_NODE_FAKE}));
    });

    it('should return false for {element:realNode} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({element:realNode}));
    });

    it('should return FALSE for {element:OBJ, attribute:"data-empty"} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({element:input.OBJ, attribute:'data-empty'}));
    });

    it('should return FALSE for {element:input.OBJ, name:"fake"} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({element:input.OBJ, name:'fake'}));
    });

    it('should return FALSE for {name: "foobar"} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton({name:'foobar'}));
    });

    it('should return FALSE for {name: "foobar"} input', function() {
      assert.deepStrictEqual(false, prog.toggleTextToggleButton(textToggleButtonBad));
    });

    prog.toggleTextToggleButton(textToggleButtonTrue);
    propertiesSetCorrectly =
        textToggleButtonTrue.element.getAttribute(textToggleButtonTrue.attribute) === textToggleButtonTrue.attributeState2 &&
        textToggleButtonTrue.element.innerHTML === textToggleButtonTrue.notPressedText;

    it('should return TRUE for textToggleButtonTrue toggled to state2 input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });

    prog.toggleTextToggleButton(textToggleButtonFalse);
    propertiesSetCorrectly =
        textToggleButtonFalse.element.getAttribute(textToggleButtonFalse.attribute) === textToggleButtonFalse.attributeState1 &&
        textToggleButtonFalse.element.innerHTML === textToggleButtonFalse.pressedText;

    it('should return TRUE for textToggleButtonTrue toggled to state1 input', function() {
      assert.deepStrictEqual(true, propertiesSetCorrectly);
    });
  }
};
