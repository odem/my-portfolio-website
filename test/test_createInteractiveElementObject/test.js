/** createInteractiveElementObj =========================================================== */

/** Initialization  ------------------------------ */
/** jsdom prog. */
const jsdom = require('jsdom');

/** Inputs  ------------------------------ */
/** DOM Object. */
const { JSDOM } = jsdom;
/** Virual DOM. */
const dom = new JSDOM(
  '<!DOCTYPE html><html><head></head><body>' +
    '<div id="foobar"></div>' +
  '</body></html>');
/** Virtual DOM window. */
const window = dom.window;
/** Virtual DOM document. */
const document = dom.window.document;
/** Real (virtual) DOM node. */
const realNode = document.getElementById('foobar');
/** InteractiveElement Objects */
const interactiveElement = {
  name: 'interactiveElement',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
};
const interactiveElementName = {
  name: 'foobar',
  attribute: 'aria-pressed',
  attributeState1: 'true',
  attributeState2: 'false',
  element: realNode,
};

/** Test  ------------------------------ */
module.exports = {
  createInteractiveElementObjTest: function createInteractiveElementObjTest(assert, prog, input) {
    it('Should return {} for true input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(true));
    });

    it('Should return {} for false input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(false));
    });

    it('Should return {} for null input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(null));
    });

    it('Should return {} for undefined input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(undefined));
    });

    it('Should return {} for NaN input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(NaN));
    });

    it('Should return {} for INT_POS input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.INT_POS));
    });

    it('Should return {} for INT_NEG input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.INT_NEG));
    });

    it('Should return {} for INT_ZERO_POS input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.INT_ZERO_POS));
    });

    it('Should return {} for INT_ZERO_NEG input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.INT_ZERO_NEG));
    });

    it('Should return {} for FLOAT_POS input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.FLOAT_POS));
    });

    it('Should return {} for FLOAT_NEG input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.FLOAT_NEG));
    });

    it('Should return {} for FLOAT_ZERO_POS input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.FLOAT_ZERO_POS));
    });

    it('Should return {} for FLOAT_ZERO_NEG input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.FLOAT_ZERO_NEG));
    });

    it('Should return {} for STR_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.STR_EMPTY));
    });

    it('Should return {} for STR_LONG input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.STR_LONG));
    });

    it('Should return {} for ARRAY_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_EMPTY));
    });

    it('Should return {} for ARRAY_NUM input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_NUM));
    });

    it('Should return {} for ARRAY_STR input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_STR));
    });

    it('Should return {} for ARRAY_ARRAY_NUM input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_ARRAY_NUM));
    });

    it('Should return {} for ARRAY_ARRAY_STR input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_ARRAY_STR));
    });

    it('Should return {} for ARRAY_OBJ_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_OBJ_EMPTY));
    });

    it('Should return {} for ARRAY_OBJ input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ARRAY_OBJ));
    });

    it('Should return {} for OBJ input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.OBJ));
    });

    it('Should return {} for OBJ_EMPTY input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.OBJ_EMPTY));
    });

    it('Should return {} for ELEMENT_NODE_FAKE input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj(input.ELEMENT_NODE_FAKE));
    });

    it('Should return {} for {element:input.OBJ} input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj({element:input.OBJ}));
    });

    it('Should return {} for {element:input.ELEMENT_NODE_FAKE} input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj({element:input.ELEMENT_NODE_FAKE}));
    });

    it('Should return {} for {element:OBJ, attribute:"data-empty"} input', function() {
      assert.deepStrictEqual({},
          prog.createInteractiveElementObj({element:input.OBJ, attribute:'data-empty'}));
    });

    it('Should return {} for {element:input.OBJ, name: "fake"} input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj({element:input.OBJ, name:'fake'}));
    });

    it('Should return {} for {name: "fake"} input', function() {
      assert.deepStrictEqual({}, prog.createInteractiveElementObj({name:'fake'}));
    });

    it('Should return interactiveElement for realNode input', function() {
      assert.deepStrictEqual(interactiveElement, prog.createInteractiveElementObj(realNode));
    });

    it('Should return interactiveElement for {element:realNode} input', function() {
      assert.deepStrictEqual(interactiveElement, prog.createInteractiveElementObj({element:realNode}));
    });

    it('Should return interactiveElementName for {element:foobarNode, name:"foobar"} input', function() {
      assert.deepStrictEqual(interactiveElementName, prog.createInteractiveElementObj({element:realNode, name:'foobar'}));
    });
  }
};
