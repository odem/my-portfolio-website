# My Portfolio Website
This site highlights my professional career, and the advancement of my web development skills. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.  

July 2018

## Demo
https://www.shanaodem.com

## My Roles
 - Design
 - User-Experience
 - Front-End Development
 - Back-End Development

## Tools/Skills Used
 - HTML5
 - SCSS
 - JavaScript
 - PHP
 - Mocha
 - Gulp

## File Structure
The `src` folder contains the raw `.scss` and `.js` files that get compiled into final production files.
The `dist` folder contains a complete build of all production files.

## Site Build
**Automatic Build:**     Gulp  
**Browser Test:**        BrowserSync  
**JavaScript Lint:**     ESLint  
**CSS Build:**           Sass  

### Dependencies
 - browser-sync
 - del
 - eslint
 - eslint-config-google
 - gulp
 - gulp-clean-css
 - gulp-concat
 - gulp-eslint
 - gulp-rename
 - gulp-sass
 - gulp-uglify


### Usage
Complete build (and file-change watch): 
```
gulp
```

Build only sass files into CSS files: 
```
gulp sass
```

Minify only CSS files: 
```
gulp css
```

Lint only JavaScript files: 
```
gulp lint
```

Build JavaScript files (lint, combine, and minify): 
```
gulp scripts
```

## Site Test
**Framework:**   Mocha  
**Virtual Dom:** jsdom  

### Dependencies
 - jsdom

### Usage
```
cd test
npm test
```
