/** @type {Object} gulp task build. */
var gulp = require('gulp');
/** @type {Object} file and folder deletion using globs. */
var del = require('del');
/** @type {Object} file concatenation. */
var concat = require('gulp-concat');
/** @type {Object} file rename. */
var rename = require('gulp-rename');
/** @type {Object} JavaScript linting. */
var eslint = require('gulp-eslint');
/** @type {Object} minify JavaScript. */
var jsmin = require('gulp-uglify');
/** @type {Object} sass build. */
var sass = require('gulp-sass');
/** @type {Object} minify CSS. */
var cssmin = require('gulp-clean-css');
/** @type {Object} browser and device sync. */
var server = require('browser-sync').create();
/** @type {Object} default tasks when 'gulp' typed in CLI. */
var defaultTasks = null;
/** @type {Array} holds only distribution file paths. */
var files = [
  'dist/*.html',
  'dist/css/*.css',
  'dist/js/*.js',
];
/** @type {Object} holds all relevant file paths. */
var paths = {
  scripts: {
    src: 'src/js/main.js',
    dest: 'dist/js',
    concat: 'main.js',
    minify: 'main_min.js',
  },
  css: {
    src: 'src/scss/*.scss',
    dest: 'dist/css',
    scss: 'src/scss/**/*.scss',
    main: 'dist/css/main.css',
    print: 'dist/css/print.css',
  },
  html: 'dist/*.html',
  dist: {
    all: 'dist/*',
    css: 'dist/css/*.css',
    js: 'dist/js/*.js',
  },
};

/**
 * Initializes BrowserSync by serving files from dist folder.
 * @param  {Function} done - ends initialization.
 */
function serve(done) {
  server.init(files, {
    server: {
      baseDir: './dist',
    },
  });
  done();
}

/**
 * Reloads BrowserSync
 * @param  {Function} done - ends reloading.
 */
function reload(done) {
  server.reload();
  done();
}

/**
 * Minifies CSS file.
 * @param  {Object} file - name of file to be minified
 */
function minifyCSS(file) {
  gulp.src(file)
    .pipe(rename({suffix: '_min'}))
    .pipe(cssmin())
    .pipe(gulp.dest(paths.css.dest));
}

/** Cleans distribution folder. */
gulp.task('clean', function() {
  return del([paths.dist.css, paths.dist.js]);
});

/** Lints JavaScript. */
gulp.task('lint', function() {
  return gulp.src(paths.scripts.src)
    .pipe(eslint())
    /** Outputs the lint results to the console. */
    .pipe(eslint.format());
});

/** Builds/Compiles Sass file into CSS and minifies CSS. */
gulp.task('sass', function() {
  return gulp.src(paths.css.src)
    .pipe(sass())
    .pipe(gulp.dest(paths.css.dest));
});

/** Minifies CSS files. */
gulp.task('css', function(done) {
  minifyCSS(paths.css.main);
  minifyCSS(paths.css.print);
  done();
});

/** Concatenates and minifies JavaScript. */
gulp.task('scripts', function() {
  return gulp.src(paths.scripts.src)
    .pipe(concat(paths.scripts.concat))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(rename(paths.scripts.minify))
    .pipe(jsmin())
    .pipe(gulp.dest(paths.scripts.dest));
});

/** Watches for file changes. */
gulp.task('watch', function() {
  gulp.watch(paths.scripts.src, gulp.series('lint', 'scripts', reload));
  gulp.watch(paths.css.scss, gulp.series('sass', 'css', reload));
  gulp.watch(paths.html, gulp.series(reload));
});

defaultTasks =
    gulp.series('clean', 'lint', 'scripts', 'sass', 'css', serve, 'watch');
gulp.task('default', defaultTasks);
