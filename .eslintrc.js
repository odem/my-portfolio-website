module.exports = { 
    "env": {
        "browser": true,
        "commonjs": true
    },
    "extends": "google",
    "rules": {
        "no-mixed-spaces-and-tabs": "error",
        "no-tabs": "error",
        "no-use-before-define": "error",
        "no-var": "off"
    } 
};
